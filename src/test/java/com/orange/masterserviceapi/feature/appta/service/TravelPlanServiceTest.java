package com.orange.masterserviceapi.feature.appta.service;

import java.util.ArrayList;
import java.util.List;

import com.orange.masterserviceapi.common.exception.MasterServiceApiBusinessException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlan;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlanDetail;
import com.orange.masterserviceapi.datasource.entities.MstTaPlan;
import com.orange.masterserviceapi.datasource.entities.MstTaPlanMapCoverage;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanDetailRepo;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanRepo;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanMapCoverageRepo;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanRepo;

@TestComponent
public class TravelPlanServiceTest {
	@InjectMocks
	private TravelPlanService travelPlanServiceMock;
	@Mock
	private MstTaCoveragePlanDetailRepo mstTaCoveragePlanDetailRepoMock;
	@Mock
	private MstTaCoveragePlanRepo mstTaCoveragePlanRepoMock;
	@Mock
	private MstTaPlanRepo mstTaPlanRepoMock;
	@Mock
	private MstTaPlanMapCoverageRepo mstTaPlanMapCoverageRepoMock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

    @Test
    public void getTravelPlan_Success() throws MasterServiceApiException {
        //Given
        List<MstTaPlan> listTaPlan = new ArrayList<>();
        MstTaPlan mstTaPlan = new MstTaPlan();
        listTaPlan.add(mstTaPlan);
        List<MstTaPlanMapCoverage> listTaPlanMapConverage = new ArrayList<>();
        List<MstTaCoveragePlanDetail> listCoveragePlanDetail = new ArrayList<>();
        List<MstTaCoveragePlan> listCoveragePlan = new ArrayList<>();
        Mockito.when(mstTaPlanRepoMock.findAllByOrderByPlanCodeAsc()).thenReturn(listTaPlan);
        Mockito.when(mstTaPlanMapCoverageRepoMock.findAllByOrderByPlanCodeAsc()).thenReturn(listTaPlanMapConverage);
        Mockito.when(mstTaCoveragePlanDetailRepoMock.findAllByOrderByCoveragePlanGroupAscCoveragePlanDetailCodeAsc())
                .thenReturn(listCoveragePlanDetail);
        Mockito.when(mstTaCoveragePlanRepoMock.findAllByOrderByCoverageCodeAsc()).thenReturn(listCoveragePlan);

        //When
        GenericResponse response = this.travelPlanServiceMock.getTravelPlan(4);

        //Then
        Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
    }

    @Test
    public void getTravelPlan_TaPlanDataNotFound() throws MasterServiceApiException {
        //Given
        List<MstTaPlan> listTaPlan = new ArrayList<>();
        Mockito.when(mstTaPlanRepoMock.findAllByOrderByPlanCodeAsc()).thenReturn(listTaPlan);
        try{
            //When
            GenericResponse	response = this.travelPlanServiceMock.getTravelPlan(2);
            Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
        } catch (MasterServiceApiDataNotFoundException e) {
            //Then
            Assert.assertEquals(e.getStatus().getCode(), ResultCode.DATA_NOT_FOUND.getCode());
        }
    }

    @Test
    public void getTravelPlan_WhenDateAmountLessThanZero() throws MasterServiceApiException {
        //Given
        List<MstTaPlan> listTaPlan = new ArrayList<>();
        Mockito.when(mstTaPlanRepoMock.findAllByOrderByPlanCodeAsc()).thenReturn(listTaPlan);
        try{
            //When
            GenericResponse	response = this.travelPlanServiceMock.getTravelPlan(-1);
            Assert.assertEquals(ResultCode.INVALID_BUSINESS.getCode(), response.getStatus().getCode());
        } catch (MasterServiceApiBusinessException e) {
            //Then
            Assert.assertEquals(e.getStatus().getCode(), ResultCode.INVALID_BUSINESS.getCode());
        }
    }

    @Test
    public void getTravelPlan_WhenDateAmountEqualsZero() throws MasterServiceApiException {
        //Given
        List<MstTaPlan> listTaPlan = new ArrayList<>();
        Mockito.when(mstTaPlanRepoMock.findAllByOrderByPlanCodeAsc()).thenReturn(listTaPlan);
        try{
            //When
            GenericResponse	response = this.travelPlanServiceMock.getTravelPlan(0);
            Assert.assertEquals(ResultCode.INVALID_BUSINESS.getCode(), response.getStatus().getCode());
        } catch (MasterServiceApiBusinessException e) {
            //Then
            Assert.assertEquals(e.getStatus().getCode(), ResultCode.INVALID_BUSINESS.getCode());
        }
    }
}
