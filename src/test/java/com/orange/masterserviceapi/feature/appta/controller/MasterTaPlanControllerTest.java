package com.orange.masterserviceapi.feature.appta.controller;

import static org.mockito.Mockito.when;

import java.util.Optional;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTravelPlantRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaPlan;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanRepo;
import com.orange.masterserviceapi.feature.appta.service.MasterTaPlanService;

@TestComponent
public class MasterTaPlanControllerTest {
	@InjectMocks
	private MasterTaPlanController masterTaPlanControllerMock;
	@Mock
	private MasterTaPlanService masterTaPlanServiceMock;
	@Mock
	private MstTaPlanRepo mstTaPlanRepoMock;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController mstTaPlanMapRequest = new MstTravelPlantRequestController();
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		Mockito.when(
				this.masterTaPlanServiceMock.insertMstTravelPlan(Mockito.any(MstTravelPlantRequestController.class)))
				.thenReturn(response);

		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanControllerMock.insertTravelPlan("X-000",
				mstTaPlanMapRequest);

		// Then
		Assert.assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}

	@Test
	public void updateTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController reqeust = new MstTravelPlantRequestController();
		reqeust.setPlanName("test");
		reqeust.setPlanPrice(Double.valueOf(100));
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		MstTaPlan mstTaPlan = new MstTaPlan();
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanControllerMock.updateTravelPlan("X-000",
				new String(""), reqeust);

		// Then
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	@Test
	public void deleteTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController reqeust = new MstTravelPlantRequestController();
		reqeust.setPlanName("test");
		reqeust.setPlanPrice(Double.valueOf(100));
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		MstTaPlan mstTaPlan = new MstTaPlan();
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanControllerMock.deleteTravelPlan("X-000",
				new String(""));

		// Then
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	@Test
	public void searchTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController reqeust = new MstTravelPlantRequestController();
		reqeust.setPlanName("test");
		reqeust.setPlanPrice(Double.valueOf(100));
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		MstTaPlan mstTaPlan = new MstTaPlan();
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanControllerMock.searchTravelPlan("X-000",
				new String(""));

		// Then
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}
}
