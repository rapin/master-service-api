package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.appta.service.TravelPlanService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@TestComponent
public class TravelPlanControllerTest {
    @InjectMocks private TravelPlanController travelPlanController;
    @Mock private TravelPlanService travelPlanService;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllPlanSuccess() throws Exception {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.travelPlanService.getTravelPlan(Mockito.anyInt())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.travelPlanController.getAllPlan("X-000", 3);

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}

