package com.orange.masterserviceapi.feature.appta.service;

import static org.mockito.ArgumentMatchers.anyString;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlan;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanRepo;

@TestComponent
public class MasterTaCoverageDaoPlanServiceTest {
	@InjectMocks
	private MasterTaCoveragePlanService masterTaCoveragePlanServiceMock;
	@Mock
	private MstTaCoveragePlanRepo mstTaCoveragePlanRepoMock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertMstTaCoveragePlanCoverageCodeIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("");
		req.setCoverageName("ความคุ้มครองชีวิต");
		req.setIsActive("Y");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.insertMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTaCoveragePlanCoverageNameIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP001");
		req.setCoverageName("");
		req.setIsActive("N");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.insertMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void insertMstTaCoveragePlanIsActiveIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP001");
		req.setCoverageName("ความคุ้มครองชีวิต");
		req.setIsActive("");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.insertMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void insertMstTaCoveragePlanIsActiveIsInvalid() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP001");
		req.setCoverageName("ความคุ้มครองชีวิต");
		req.setIsActive("Z");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.insertMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTaCoveragePlanSuccess() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP001");
		req.setCoverageName("ความคุ้มครองชีวิต");
		req.setIsActive("N");

		// When
		GenericResponse response = this.masterTaCoveragePlanServiceMock.insertMstTaCoveragePlan(req);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}
	
	@Test
	public void updateMstTaCoveragePlanCoverageCodeNotFound() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP007");
		req.setCoverageName("คุ้มครองเพิ่มเติม77");
		req.setIsActive("Y");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.updateMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataNotFoundException e) {
			// Then
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanCoverageNameIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP008");
		req.setCoverageName("");
		req.setIsActive("Y");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.updateMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanStatusIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP008");
		req.setCoverageName("ความคุ้มครองชีวิต123");
		req.setIsActive("");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.updateMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanStatusNotMatch() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP008");
		req.setCoverageName("ความคุ้มครองชีวิต123");
		req.setIsActive("Z");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.updateMstTaCoveragePlan(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanSuccess() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanRequestController req = new MstTaCoveragePlanRequestController();
		req.setCoverageCode("CP008");
		req.setCoverageName("ความคุ้มครองชีวิต123");
		req.setIsActive("Y");
		
		MstTaCoveragePlan mstTaCoveragePlan = new MstTaCoveragePlan();
    	Mockito.when(this.mstTaCoveragePlanRepoMock.findByCoverageCode(anyString())).thenReturn(mstTaCoveragePlan);

		// When
		GenericResponse response = this.masterTaCoveragePlanServiceMock.updateMstTaCoveragePlan(req);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}
	
	@Test
	public void deleteMstTaCoveragePlanCoverageCodeNotFound() throws MasterServiceApiException {
		// Given
    	Mockito.when(this.mstTaCoveragePlanRepoMock.findByCoverageCode(anyString())).thenReturn(null);
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanServiceMock.deleteMstTaCoveragePlan("CP020");
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataNotFoundException e) {
			// Then
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void deleteMstTaCoveragePlanSuccess() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlan mstTaCoveragePlan = new MstTaCoveragePlan();
    	Mockito.when(this.mstTaCoveragePlanRepoMock.findByCoverageCode(anyString())).thenReturn(mstTaCoveragePlan);

		// When
		GenericResponse response = this.masterTaCoveragePlanServiceMock.deleteMstTaCoveragePlan("CP008");

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}
	
	@Test
    public void searchMstTaCoveragePlanNotFound() throws MasterServiceApiException {
        //Given
    	Mockito.when(this.mstTaCoveragePlanRepoMock.findByCoverageCode(anyString())).thenReturn(null);

        try {
            //When
            GenericResponse response = this.masterTaCoveragePlanServiceMock.searchMstTaCoveragePlan("CP020");
            Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(),response.getStatus().getCode());
        }catch (MasterServiceApiDataNotFoundException e){
            //Then
            Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(),e.getStatus().getCode());
        }
    }

    @Test
    public void searchMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
    	MstTaCoveragePlan mstTaCoveragePlan = new MstTaCoveragePlan();
    	Mockito.when(this.mstTaCoveragePlanRepoMock.findByCoverageCodeAndIsActiveEquals(anyString(),anyString())).thenReturn(mstTaCoveragePlan);

        //When
        GenericResponse response = this.masterTaCoveragePlanServiceMock.searchMstTaCoveragePlan("CP008");

        //Then
        Assert.assertEquals(ResultCode.SUCCESS.getCode(),response.getStatus().getCode());
    }

	
}
