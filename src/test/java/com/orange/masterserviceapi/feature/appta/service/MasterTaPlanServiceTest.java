package com.orange.masterserviceapi.feature.appta.service;

import static org.mockito.Mockito.when;

import java.util.Optional;

import com.orange.masterserviceapi.common.utils.ConstantUtils;
import com.orange.masterserviceapi.feature.appta.controller.domain.MstTravelPlantRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaPlan;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanRepo;

@TestComponent
public class MasterTaPlanServiceTest {
	@InjectMocks
	private MasterTaPlanService masterTaPlanServiceMock;
	@Mock
	private MstTaPlanRepo mstTaPlanRepoMock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertMstTravelPlanPlanCodeIsEmpty() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController Req = new MstTravelPlantRequestController();
		Req.setPlanCode("");
		Req.setPlanName(ConstantUtils.Y_FLAG);
		Req.setPlanPrice((double) 100);
		try {
			// When
			GenericResponse response = this.masterTaPlanServiceMock.insertMstTravelPlan(Req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTravelPlanPlanNameIsEmpty() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController Req = new MstTravelPlantRequestController();
		Req.setPlanCode("PC001");
		Req.setPlanName("");
		Req.setPlanPrice((double) 100);
		try {
			// When
			GenericResponse response = this.masterTaPlanServiceMock.insertMstTravelPlan(Req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController req = new MstTravelPlantRequestController();
		req.setPlanCode("PC001");
		req.setPlanName("D");
		req.setPlanPrice((double) 105);
		req.setIsActive(ConstantUtils.Y_FLAG);

		// When
		GenericResponse response = this.masterTaPlanServiceMock.insertMstTravelPlan(req);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void updateMstTravelPlan_Success() throws MasterServiceApiException {
		// Given
		MstTravelPlantRequestController reqeust = new MstTravelPlantRequestController();
		reqeust.setPlanName("test");
		reqeust.setPlanPrice(Double.valueOf(100));
		reqeust.setIsActive(ConstantUtils.Y_FLAG);
		MstTaPlan mstTaPlan = new MstTaPlan();
		// When
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// Then
		GenericResponse response = this.masterTaPlanServiceMock.updateMstTravelPlan(reqeust, new String(""));
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void updateMstTravelPlan_DataNotFound() {
		// Given
		MstTravelPlantRequestController reqeust = new MstTravelPlantRequestController();
		reqeust.setPlanName("test");
		reqeust.setPlanPrice(Double.valueOf(100));
		// When
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.empty());
		// Then
		try {
			this.masterTaPlanServiceMock.updateMstTravelPlan(reqeust, new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}

	}

	@Test
	public void deleteMstTravelPlan_DataNotFound() {
		// When
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.empty());
		// Then
		try {
			this.masterTaPlanServiceMock.deleteMstTravelPlan(new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}

	}

	@Test
	public void deleteMstTravelPlan_Success() throws MasterServiceApiException {
		MstTaPlan mstTaPlan = new MstTaPlan();
		// When
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// Then
		GenericResponse response = this.masterTaPlanServiceMock.deleteMstTravelPlan(new String(""));
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void getMstTravelPlan_Success() throws MasterServiceApiException {
		MstTaPlan mstTaPlan = new MstTaPlan();
		// When
		when(this.mstTaPlanRepoMock.findByPlanCode(Mockito.anyString())).thenReturn((mstTaPlan));
		// Then
		GenericResponse response = this.masterTaPlanServiceMock.getMstTravelPlan(new String(""));
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void getMstTravelPlan_DataNotFound() throws MasterServiceApiException {
		// When
		when(this.mstTaPlanRepoMock.findById(Mockito.anyString())).thenReturn(Optional.empty());
		// Then
		try {
			this.masterTaPlanServiceMock.getMstTravelPlan(new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}

	}
}
