package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.appta.service.MasterTaCoveragePlanService;

@TestComponent
public class MasterTaCoverageDaoPlanControllerTest {
    @InjectMocks
    private MasterTaCoveragePlanController mstTaCoveragePlanControllerMock;
    @Mock
    private MasterTaCoveragePlanService mstTaCoveragePlanServiceMock;
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void insertMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
    	MstTaCoveragePlanRequestController mstTaCoveragePlanRequest = new MstTaCoveragePlanRequestController();
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.mstTaCoveragePlanServiceMock.insertMstTaCoveragePlan(Mockito.any(MstTaCoveragePlanRequestController.class))).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanControllerMock.insertMstTaCoveragePlan("X-000", mstTaCoveragePlanRequest);

        //Then
        Assert.assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }
    
    @Test
    public void updateMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
    	MstTaCoveragePlanRequestController mstTaCoveragePlanRequest = new MstTaCoveragePlanRequestController();
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.mstTaCoveragePlanServiceMock.updateMstTaCoveragePlan(Mockito.any(MstTaCoveragePlanRequestController.class))).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanControllerMock.updateMstTaCoveragePlan("X-000", mstTaCoveragePlanRequest);

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    
    
    @Test
    public void deleteMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.mstTaCoveragePlanServiceMock.deleteMstTaCoveragePlan(Mockito.any())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanControllerMock.deleteMstTaCoveragePlan("X-000", "CP009");

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    
    @Test
    public void searchMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.mstTaCoveragePlanServiceMock.searchMstTaCoveragePlan(Mockito.any())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanControllerMock.searchMstTaCoveragePlan("X-000", "CP009");

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
