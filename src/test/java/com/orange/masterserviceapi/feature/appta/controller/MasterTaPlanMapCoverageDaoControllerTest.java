package com.orange.masterserviceapi.feature.appta.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaPlanMapCoverageRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaPlanMapCoverage;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanMapCoverageRepo;
import com.orange.masterserviceapi.feature.appta.service.MasterTaPlanMapCoverageService;

@TestComponent
public class MasterTaPlanMapCoverageDaoControllerTest {
	@InjectMocks
	private MasterTaPlanMapCoverageController masterTaPlanMapCoverageControllerMock;
	@Mock
	private MasterTaPlanMapCoverageService masterTaPlanMapCoverageServiceMock;
	@Mock
	private MstTaPlanMapCoverageRepo mstTaPlanMapCoverageRepoMock;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapRequest = new MstTaPlanMapCoverageRequestController();
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		Mockito.when(this.masterTaPlanMapCoverageServiceMock
				.insertMstTaPlanMapCoverage(Mockito.any(MstTaPlanMapCoverageRequestController.class)))
				.thenReturn(response);

		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanMapCoverageControllerMock.insertTravelPlan("X-000",
				mstTaPlanMapRequest);

		// Then
		Assert.assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}

	@Test
	public void updateTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		MstTaPlanMapCoverageRequestController reqeust = new MstTaPlanMapCoverageRequestController();
		reqeust.setCoveragePlanCode("test");
		reqeust.setCoveragePlanDetailCode("test");
		reqeust.setPlanCode("test");
		reqeust.setPrice(Double.valueOf(100));
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		MstTaPlanMapCoverage mstTaPlan = new MstTaPlanMapCoverage();
		when(this.mstTaPlanMapCoverageRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanMapCoverageControllerMock.updateTravelPlan("X-000",
				new String(""), reqeust);

		// Then
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void deleteTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		MstTaPlanMapCoverage mstTaPlan = new MstTaPlanMapCoverage();
		when(this.mstTaPlanMapCoverageRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(mstTaPlan));
		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanMapCoverageControllerMock.deleteTravelPlan("X-000",
				new String(""));

		// Then
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}

	@Test
	public void searchTravelPlanSuccess() throws MasterServiceApiException {
		// Given
		List<MstTaPlanMapCoverage> mstTaPlan = new ArrayList<MstTaPlanMapCoverage>();
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.SUCCESS);
		when(this.mstTaPlanMapCoverageRepoMock.findByPlanCode(Mockito.anyString())).thenReturn(mstTaPlan);
		// When
		ResponseEntity<GenericResponse> result = this.masterTaPlanMapCoverageControllerMock.searchTravelPlan("X-000",
				new String(""));

		// Then
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
	}
}
