package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanDetailRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.appta.service.MasterTaCoveragePlanDetailService;

@TestComponent
public class MasterTaCoverageDaoPlanDetailDaoControllerTest {
    @InjectMocks
    private MasterTaCoveragePlanDetailController mstTaCoveragePlanDetailControllerMock;
    @Mock
    private MasterTaCoveragePlanDetailService mstTaCoveragePlanDetailServiceMock;
    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void insertMstTaCoveragePlanDetailSuccess() throws MasterServiceApiException {
        //Given
    	MstTaCoveragePlanDetailRequestController mstTaCoveragePlanDetailRequestController = new MstTaCoveragePlanDetailRequestController();
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        //When
        Mockito.when(this.mstTaCoveragePlanDetailServiceMock.insertMstTaCoveragePlanDetail(Mockito.any(MstTaCoveragePlanDetailRequestController.class))).thenReturn(response);
        //Then
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanDetailControllerMock.insertMstTaCoveragePlanDetail("X-000", mstTaCoveragePlanDetailRequestController);
        Assert.assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }
    
    @Test
    public void updateMstTaCoveragePlanDetailSuccess() throws MasterServiceApiException {
        //Given
    	MstTaCoveragePlanDetailRequestController mstTaCoveragePlanDetailRequestController = new MstTaCoveragePlanDetailRequestController();
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        //When
        Mockito.when(this.mstTaCoveragePlanDetailServiceMock.updateMstTaCoveragePlanDetail(Mockito.any(MstTaCoveragePlanDetailRequestController.class))).thenReturn(response);
        //Then
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanDetailControllerMock.updateMstTaCoveragePlanDetail("X-000", mstTaCoveragePlanDetailRequestController);
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    
    @Test
    public void deleteMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.mstTaCoveragePlanDetailServiceMock.deleteMstTaCoveragePlanDetail(Mockito.any())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanDetailControllerMock.deleteMstTaCoveragePlanDetail("X-000", "CPD010");

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    
    @Test
    public void searchMstTaCoveragePlanSuccess() throws MasterServiceApiException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.mstTaCoveragePlanDetailServiceMock.searchMstTaCoveragePlanDetail(Mockito.any())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.mstTaCoveragePlanDetailControllerMock.searchMstTaCoveragePlanDetail("X-000", "CPD010");

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
