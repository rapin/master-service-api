package com.orange.masterserviceapi.feature.appta.service;

import static org.mockito.ArgumentMatchers.anyString;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanDetailRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlanDetail;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanDetailRepo;

@TestComponent
public class MasterTaCoverageDaoPlanDetailDaoServiceTest {
	@InjectMocks
	private MasterTaCoveragePlanDetailService masterTaCoveragePlanDetailServiceMock;
	@Mock
	private MstTaCoveragePlanDetailRepo mstTaCoveragePlanDetailRepoMock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertMstTravelPlanDetailSuccess() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("001");
		req.setCoveragePlanDetailName("testทดสอบ");
		req.setCoveragePlanGroup("CP001");

		// When
		GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.insertMstTaCoveragePlanDetail(req);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void insertMstTravelPlanDetail_ValidateCoveragePlanDetailCode() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("");
		req.setCoveragePlanDetailName("ความสูญเสียหรือความเสียหายของเอกสารการเดินทาง");
		req.setCoveragePlanGroup("CP003");

		// When
		try {
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.insertMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.FAIL.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException ex) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), ex.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTravelPlanDetail_ValidateCoveragePlanDetailName() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("CPD009");
		req.setCoveragePlanDetailName("");
		req.setCoveragePlanGroup("CP003");

		// When
		try {
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.insertMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.FAIL.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException ex) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), ex.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTravelPlanDetail_ValidateCoveragePlanGroup() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("CPD009");
		req.setCoveragePlanDetailName("ความสูญเสียหรือความเสียหายของเอกสารการเดินทาง");
		req.setCoveragePlanGroup("");

		// When
		try {
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.insertMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.FAIL.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException ex) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), ex.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanDetailCoverageDetailCodeNotFound() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("001");
		req.setCoveragePlanDetailName("testทดสอบ");
		req.setCoveragePlanGroup("CP001");
		req.setIsActive("Y");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.updateMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataNotFoundException e) {
			// Then
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanDetailCoverageDetailNameIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("CPD010");
		req.setCoveragePlanDetailName("");
		req.setCoveragePlanGroup("CP001");
		req.setIsActive("Y");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.updateMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanDetailCoveragePlanGroupIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("CPD010");
		req.setCoveragePlanDetailName("testทดสอบ");
		req.setCoveragePlanGroup("");
		req.setIsActive("Y");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.updateMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanDetailStatusIsEmpty() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("CPD010");
		req.setCoveragePlanDetailName("testทดสอบ");
		req.setCoveragePlanGroup("CP001");
		req.setIsActive("");
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.updateMstTaCoveragePlanDetail(req);
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataValidateException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void updateMstTaCoveragePlanDetailSuccess() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetailRequestController req = new MstTaCoveragePlanDetailRequestController();
		req.setCoveragePlanDetailCode("CPD010");
		req.setCoveragePlanDetailName("testทดสอบ");
		req.setCoveragePlanGroup("CP001");
		req.setIsActive("Y");
		
		MstTaCoveragePlanDetail mstTaCoveragePlanDetail = new MstTaCoveragePlanDetail();
    	Mockito.when(this.mstTaCoveragePlanDetailRepoMock.findByCoveragePlanDetailCode(anyString())).thenReturn(mstTaCoveragePlanDetail);

		// When
		GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.updateMstTaCoveragePlanDetail(req);

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}
	
	@Test
	public void deleteMstTaCoveragePlanDetailCoverageCodeNotFound() throws MasterServiceApiException {
		// Given
    	Mockito.when(this.mstTaCoveragePlanDetailRepoMock.findByCoveragePlanDetailCode(anyString())).thenReturn(null);
		try {
			// When
			GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.deleteMstTaCoveragePlanDetail("CPD020");
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiDataNotFoundException e) {
			// Then
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}
	
	@Test
	public void deleteMstTaCoveragePlanDetailSuccess() throws MasterServiceApiException {
		// Given
		MstTaCoveragePlanDetail mstTaCoveragePlanDetail= new MstTaCoveragePlanDetail();
    	Mockito.when(this.mstTaCoveragePlanDetailRepoMock.findByCoveragePlanDetailCode(anyString())).thenReturn(mstTaCoveragePlanDetail);

		// When
		GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.deleteMstTaCoveragePlanDetail("CPD008");

		// Then
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}
	
	@Test
    public void searchMstTaCoveragePlanDetailNotFound() throws MasterServiceApiException {
        //Given
    	Mockito.when(this.mstTaCoveragePlanDetailRepoMock.findByCoveragePlanDetailCode(anyString())).thenReturn(null);

        try {
            //When
            GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.searchMstTaCoveragePlanDetail("CP020");
            Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(),response.getStatus().getCode());
        }catch (MasterServiceApiDataNotFoundException e){
            //Then
            Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(),e.getStatus().getCode());
        }
    }

    @Test
    public void searchMstTaCoveragePlanDetailSuccess() throws MasterServiceApiException {
        //Given
    	MstTaCoveragePlanDetail mstTaCoveragePlanDetail = new MstTaCoveragePlanDetail();
    	Mockito.when(this.mstTaCoveragePlanDetailRepoMock.findByCoveragePlanDetailCode(anyString())).thenReturn(mstTaCoveragePlanDetail);

        //When
        GenericResponse response = this.masterTaCoveragePlanDetailServiceMock.searchMstTaCoveragePlanDetail("CP008");

        //Then
        Assert.assertEquals(ResultCode.SUCCESS.getCode(),response.getStatus().getCode());
    }
}
