package com.orange.masterserviceapi.feature.appta.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaPlanMapCoverageRequestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaPlanMapCoverage;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanMapCoverageRepo;

@TestComponent
public class MasterTaPlanMapCoverageDaoServiceTest {
	@InjectMocks
	private MasterTaPlanMapCoverageService masterTaPlanMapCoverageServiceMock;
	@Mock
	private MstTaPlanMapCoverageRepo mstTaPlanMapCoverageRepoMock;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertMstTaPlanMapCoverage_Success() {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest = new MstTaPlanMapCoverageRequestController();
		mstTaPlanMapCoverageRequest.setCoveragePlanCode("test");
		mstTaPlanMapCoverageRequest.setCoveragePlanDetailCode("test");
		mstTaPlanMapCoverageRequest.setPlanCode("test");
		mstTaPlanMapCoverageRequest.setPrice(Double.valueOf(100));
		try {
			// When
			GenericResponse response = this.masterTaPlanMapCoverageServiceMock
					.insertMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest);
			Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
		} catch (MasterServiceApiException e) {
			// Then
			Assert.assertEquals(true, false);
		}
	}

	@Test
	public void insertMstTaPlanMapCoverage_PlanCodeIsEmpty() {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest = new MstTaPlanMapCoverageRequestController();
		mstTaPlanMapCoverageRequest.setCoveragePlanCode("test");
		mstTaPlanMapCoverageRequest.setCoveragePlanDetailCode("test");
		mstTaPlanMapCoverageRequest.setPlanCode("");
		try {
			// When
			this.masterTaPlanMapCoverageServiceMock.insertMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest);
			Assert.assertEquals(true, false);
		} catch (MasterServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTaCoveragePlan_CoveragePlanCodeIsEmpty() {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest = new MstTaPlanMapCoverageRequestController();
		mstTaPlanMapCoverageRequest.setCoveragePlanCode("");
		mstTaPlanMapCoverageRequest.setCoveragePlanDetailCode("test");
		mstTaPlanMapCoverageRequest.setPlanCode("test");
		try {
			// When
			this.masterTaPlanMapCoverageServiceMock.insertMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest);
			Assert.assertEquals(true, false);
		} catch (MasterServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void insertMstTaCoveragePlan_CoveragePlanDetailCodeIsEmpty() {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest = new MstTaPlanMapCoverageRequestController();
		mstTaPlanMapCoverageRequest.setCoveragePlanCode("test");
		mstTaPlanMapCoverageRequest.setCoveragePlanDetailCode("");
		mstTaPlanMapCoverageRequest.setPlanCode("test");
		try {
			// When
			this.masterTaPlanMapCoverageServiceMock.insertMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest);
			Assert.assertEquals(true, false);
		} catch (MasterServiceApiException e) {
			// Then
			Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void updateMstTaPlanMapCoverage_Success() {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest = new MstTaPlanMapCoverageRequestController();
		mstTaPlanMapCoverageRequest.setPrice(Double.valueOf(100));
		MstTaPlanMapCoverage item = new MstTaPlanMapCoverage();
		// When
		when(this.mstTaPlanMapCoverageRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(item));
		// Then
		try {
			this.masterTaPlanMapCoverageServiceMock.updateMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest,
					new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}

	}

	@Test
	public void updateMstTaPlanMapCoverage_DataNotFound() {
		// Given
		MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest = new MstTaPlanMapCoverageRequestController();
		mstTaPlanMapCoverageRequest.setPrice(Double.valueOf(100));
		// When
		when(this.mstTaPlanMapCoverageRepoMock.findById(Mockito.anyString())).thenReturn(Optional.empty());
		// Then
		try {
			this.masterTaPlanMapCoverageServiceMock.updateMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest,
					new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void deleteMstTaPlanMapCoverage_Success() throws MasterServiceApiException {
		MstTaPlanMapCoverage item = new MstTaPlanMapCoverage();
		// When
		when(this.mstTaPlanMapCoverageRepoMock.findById(Mockito.anyString())).thenReturn(Optional.of(item));
		// Then
		GenericResponse response = this.masterTaPlanMapCoverageServiceMock.deleteMstTaPlanMapCoverage(new String(""));
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void deleteMstTravelPlan_DataNotFound() {
		// When
		when(this.mstTaPlanMapCoverageRepoMock.findById(Mockito.anyString())).thenReturn(Optional.empty());
		// Then
		try {
			this.masterTaPlanMapCoverageServiceMock.deleteMstTaPlanMapCoverage(new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}

	@Test
	public void getMstTaPlanMapCoverage_Success() throws MasterServiceApiException {
		List<MstTaPlanMapCoverage> itemMock = new ArrayList();
		// When
		when(this.mstTaPlanMapCoverageRepoMock.findByPlanCode(Mockito.anyString())).thenReturn((itemMock));
		// Then
		GenericResponse response = this.masterTaPlanMapCoverageServiceMock.getMstTaPlanMapCoverage(new String(""));
		Assert.assertEquals(ResultCode.SUCCESS.getCode(), response.getStatus().getCode());
	}

	@Test
	public void getMstTravelPlan_DataNotFound() throws MasterServiceApiException {
		// When
		when(this.mstTaPlanMapCoverageRepoMock.findByPlanCode(Mockito.anyString())).thenReturn(null);
		// Then
		try {
			this.masterTaPlanMapCoverageServiceMock.getMstTaPlanMapCoverage(new String(""));
		} catch (MasterServiceApiException e) {
			Assert.assertEquals(ResultCode.DATA_NOT_FOUND.getCode(), e.getStatus().getCode());
		}
	}
}
