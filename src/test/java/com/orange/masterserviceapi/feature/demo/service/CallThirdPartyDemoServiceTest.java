package com.orange.masterserviceapi.feature.demo.service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.demo.dao.OrangeDemoDao;
import com.orange.masterserviceapi.feature.demo.dao.domain.OrangeDemoResponse;
import com.sun.org.apache.xml.internal.utils.URI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class CallThirdPartyDemoServiceTest {
    @InjectMocks private CallThirdPartyDemoService demoCallThirdPartyService;
    @Mock private OrangeDemoDao orangeDemoDao;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void callThirdPartyDemoSuccess() throws MasterServiceApiException, URI.MalformedURIException {
        //Given
        Mockito.when(this.orangeDemoDao.getApplicationFromThirdPartyDemo(Mockito.any())).thenReturn(new OrangeDemoResponse());

        //When
        GenericResponse response = this.demoCallThirdPartyService.callThirdPartyDemo("APP_NUMBER");

        //Then
        Assert.assertEquals(ResultCode.SUCCESS.getCode(),response.getStatus().getCode());
    }
}
