package com.orange.masterserviceapi.feature.demo.controller;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.demo.service.CallThirdPartyDemoService;
import com.orange.masterserviceapi.feature.demo.service.SearchAllPlanDemoService;
import com.sun.org.apache.xml.internal.utils.URI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@TestComponent
public class CallThirdPartyDemoControllerTest {
    @InjectMocks
    private CallThirdPartyDemoController callThirdPartyDemoController;
    @Mock
    private CallThirdPartyDemoService demoCallThirdPartyService;
    @Mock private SearchAllPlanDemoService searchAllPlanDemoService;

    @Before
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void callThirdPartyDemoSuccess() throws MasterServiceApiException, URI.MalformedURIException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.demoCallThirdPartyService.callThirdPartyDemo(Mockito.any())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.callThirdPartyDemoController.callThirdPartyDemo("X-000", "APP_NO");

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void getAllPlanDemoSuccess() throws MasterServiceApiException {
        //Given
        GenericResponse response  = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        Mockito.when(this.searchAllPlanDemoService.getAllTaPlanDemo(Mockito.anyInt())).thenReturn(response);

        //When
        ResponseEntity<GenericResponse> result = this.callThirdPartyDemoController.getAllPlanDemo("X-000", 2);

        //Then
        Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
