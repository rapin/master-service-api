package com.orange.masterserviceapi.feature.demo.service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.demo.dao.SearchAllPlanDemoDao;
import com.orange.masterserviceapi.feature.demo.service.domain.TravelPlanResponse;
import com.orange.masterserviceapi.feature.demo.service.domain.TravelProductResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@TestComponent
public class SearchAllPlanDemoServiceTest {
    @InjectMocks private SearchAllPlanDemoService searchAllPlanDemoService;
    @Mock private SearchAllPlanDemoDao searchAllPlanDemoDao;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllTaPlanDemo() throws MasterServiceApiException {
        //Given
        int dateAmount = 2;
        TravelPlanResponse travelPlanResponse = new TravelPlanResponse();
        List<TravelProductResponse> allPlan = new ArrayList<>();
        TravelProductResponse travelProductResponse = new TravelProductResponse();
        travelProductResponse.setProductName("Plan name");
        travelProductResponse.setPrice("160.50");
        allPlan.add(travelProductResponse);
        travelPlanResponse.setAllPlan(allPlan);

        Mockito.when(searchAllPlanDemoDao.getAllPlanDemo()).thenReturn(travelPlanResponse);

        //When
        GenericResponse response = this.searchAllPlanDemoService.getAllTaPlanDemo(dateAmount);

        //Then
        Assert.assertEquals(ResultCode.SUCCESS.getCode(),response.getStatus().getCode());
    }

    @Test
    public void getAllTaPlanDemoDateAmountEqualsZero() throws MasterServiceApiException {
        //Given
        int dateAmount = 0;
        try{
            //When
            GenericResponse response = this.searchAllPlanDemoService.getAllTaPlanDemo(dateAmount);
            Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(),response.getStatus().getCode());
        }catch (MasterServiceApiDataValidateException e){
            //Then
            Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(),e.getStatus().getCode());
        }
    }

    @Test
    public void getAllTaPlanDemoDateAmountLessThanZero() throws MasterServiceApiException {
        //Given
        int dateAmount = -1;
        try{
            //When
            GenericResponse response = this.searchAllPlanDemoService.getAllTaPlanDemo(dateAmount);
            Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(),response.getStatus().getCode());
        }catch (MasterServiceApiDataValidateException e){
            //Then
            Assert.assertEquals(ResultCode.INVALID_PARAM.getCode(),e.getStatus().getCode());
        }
    }
}
