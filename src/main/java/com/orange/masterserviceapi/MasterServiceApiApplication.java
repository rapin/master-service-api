package com.orange.masterserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasterServiceApiApplication.class, args);
	}

}
