package com.orange.masterserviceapi.datasource.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@Table(name = "MST_TA_COVERAGE_PLAN_DETAIL")
public class MstTaCoveragePlanDetail {
    @Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "COVERAGE_PLAN_DETAIL_CODE")
    private String coveragePlanDetailCode;

    @Column(name = "COVERAGE_PLAN_DETAIL_NAME")
    private String coveragePlanDetailName;
    
    @Column(name = "COVERAGE_PLAN_GROUP")
    private String coveragePlanGroup;
    
    @Column(name = "IS_ACTIVE")
    private String isActive;
}
