package com.orange.masterserviceapi.datasource.entities;

import com.orange.masterserviceapi.feature.demo.dao.domain.TravelPlanModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.*;
import java.io.Serializable;

@Entity @Getter @Setter
@Table(name = "MST_TA_PLAN")
@SqlResultSetMapping(name="travelPlanModel", classes = {
        @ConstructorResult(targetClass = TravelPlanModel.class,
                columns = {
                        @ColumnResult(name="travelPlanName",type = String.class),
                        @ColumnResult(name="price",type = String.class),
                        @ColumnResult(name="coveragePlanName",type = String.class),
                        @ColumnResult(name="coveragePlanCode",type = String.class),
                        @ColumnResult(name="coverageDetailGroup",type = String.class),
                        @ColumnResult(name="coverageDetailName",type = String.class),
                        @ColumnResult(name="coverage",type = String.class),
                        @ColumnResult(name="planCode",type = String.class),
                })
})
@org.hibernate.annotations.NamedNativeQueries({
        @NamedNativeQuery(name ="findTravelPlan",
                query="SELECT " +
                        "map.PLAN_CODE planCode,"+
                        "plan.PLAN_NAME travelPlanName,"+
                        "plan.PLAN_PRICE price,"+

                        "coverage_detail.COVERAGE_PLAN_GROUP coveragePlanCode,"+
                        "coverage.COVERAGE_NAME coveragePlanName,"+
                        "coverage_detail.COVERAGE_PLAN_DETAIL_CODE coverageDetailGroup,"+
                        "coverage_detail.COVERAGE_PLAN_DETAIL_NAME coverageDetailName,"+
                        "map.PRICE coverage"+

                        " FROM MST_TA_PLAN plan," +
                        "MST_TA_PLAN_MAP_COVERAGE map," +
                        "MST_TA_COVERAGE_PLAN coverage," +
                        "MST_TA_COVERAGE_PLAN_DETAIL coverage_detail" +
                        " WHERE map.IS_ACTIVE ='Y' " +
                        " AND plan.PLAN_CODE = map.PLAN_CODE" +
                        " AND map.COVERAGE_CODE = coverage.COVERAGE_CODE" +
                        " AND map.COVERAGE_PLAN_DETAIL_CODE = coverage_detail.COVERAGE_PLAN_DETAIL_CODE" +
                        " ORDER BY plan.PLAN_CODE",
                resultSetMapping = "travelPlanModel")
})

public class MstTaPlan implements Serializable{
	private static final long serialVersionUID = -1974648296317889204L;

	@Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "PLAN_CODE")
    private String planCode;

    @Column(name = "PLAN_NAME")
    private String planName;

    @Column(name = "PLAN_PRICE")
    private double planPrice;
    
    @Column(name = "IS_ACTIVE")
    private String isActive;
}
