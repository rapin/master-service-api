package com.orange.masterserviceapi.datasource.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MST_TA_COVERAGE_PLAN")
public class MstTaCoveragePlan {
	@Id
    @Column(name = "ROW_ID")
    private String rowId;

    @Column(name = "COVERAGE_CODE")
    private String coverageCode;

    @Column(name = "COVERAGE_NAME")
    private String coverageName;
    
    @Column(name = "IS_ACTIVE")
    private String isActive;
}
