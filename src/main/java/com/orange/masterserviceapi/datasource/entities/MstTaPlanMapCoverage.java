package com.orange.masterserviceapi.datasource.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "MST_TA_PLAN_MAP_COVERAGE")
public class MstTaPlanMapCoverage {
	@Id
	@Column(name = "ROW_ID")
	private String rowId;

	@Column(name = "PLAN_CODE")
	private String planCode;

	@Column(name = "COVERAGE_CODE")
	private String coveragePlanCode;

	@Column(name = "COVERAGE_PLAN_DETAIL_CODE")
	private String coveragePlanDetailCode;

	@Column(name = "PRICE")
	private Double price;

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "PLAN_CODE", nullable = false, insertable = false, updatable = false, referencedColumnName = "PLAN_CODE")
	private MstTaPlan mstTaPlan;

    @Column(name = "IS_ACTIVE")
    private String isActive;
}
