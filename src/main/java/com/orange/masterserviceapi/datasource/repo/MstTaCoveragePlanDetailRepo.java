package com.orange.masterserviceapi.datasource.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlan;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlanDetail;

@Repository
public interface MstTaCoveragePlanDetailRepo extends JpaRepository<MstTaCoveragePlanDetail,String> {
	List<MstTaCoveragePlanDetail> findAllByOrderByCoveragePlanGroupAscCoveragePlanDetailCodeAsc();
	
	public MstTaCoveragePlanDetail findByCoveragePlanDetailCode(String coveragePlanDetailCode);
}
