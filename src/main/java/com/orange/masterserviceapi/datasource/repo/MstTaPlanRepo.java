package com.orange.masterserviceapi.datasource.repo;

import com.orange.masterserviceapi.feature.demo.dao.domain.TravelPlanModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.orange.masterserviceapi.datasource.entities.MstTaPlan;

import java.util.List;

@Repository
public interface MstTaPlanRepo extends JpaRepository<MstTaPlan, String> {

	@Query(name = "findTravelPlan", nativeQuery = true)
	public List<TravelPlanModel> findTravelPlan();

	List<MstTaPlan> findAllByOrderByPlanCodeAsc();

	MstTaPlan findByPlanCode(String planCode);

}
