package com.orange.masterserviceapi.datasource.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orange.masterserviceapi.datasource.entities.MstTaPlanMapCoverage;

@Repository
public interface MstTaPlanMapCoverageRepo extends JpaRepository<MstTaPlanMapCoverage, String> {

	List<MstTaPlanMapCoverage> findAllByOrderByPlanCodeAsc();

	//List<MstTaPlanMapCoverage> findByPlanCodeAndActiveFlagTrue(String planCode);

	List<MstTaPlanMapCoverage> findByPlanCode(String planCode);

}
