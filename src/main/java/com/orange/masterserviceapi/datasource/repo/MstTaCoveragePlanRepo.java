package com.orange.masterserviceapi.datasource.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlan;

@Repository
public interface MstTaCoveragePlanRepo extends JpaRepository<MstTaCoveragePlan, String> {
	public List<MstTaCoveragePlan> findAllByOrderByCoverageCodeAsc();

	public MstTaCoveragePlan findByCoverageCode(String coverageCode);

	public MstTaCoveragePlan findByCoverageCodeAndIsActiveEquals(String coverageCode, String active);
}
