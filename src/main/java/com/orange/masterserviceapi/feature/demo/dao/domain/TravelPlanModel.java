package com.orange.masterserviceapi.feature.demo.dao.domain;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.*;

@Data
@AllArgsConstructor
@Builder @Getter @Setter
@JsonDeserialize(builder = TravelPlanModel.TravelPlanBuilder.class)
public class TravelPlanModel {

    private String travelPlanName;
    private String price;

    private String coveragePlanName;
    private String coveragePlanCode;

    private String coveragePlanDetailGroup;
    private String coveragePlanDetailName;

    private String coverage;
    private String planCode;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TravelPlanBuilder {
    }
}
