package com.orange.masterserviceapi.feature.demo.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CoverageResponse {

    @JsonProperty("coverage_name")
    private String coverageName;

    @JsonProperty("coverage_code")
    private String coverageCode;

    @JsonProperty("coverage_detail")
    private List<CoverageDetailResponse> coverageDetailResponseList;
}
