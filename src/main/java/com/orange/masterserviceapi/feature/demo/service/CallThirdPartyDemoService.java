package com.orange.masterserviceapi.feature.demo.service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.demo.dao.OrangeDemoDao;
import com.orange.masterserviceapi.feature.demo.dao.domain.OrangeDemoResponse;
import com.sun.org.apache.xml.internal.utils.URI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CallThirdPartyDemoService {
    @Autowired private OrangeDemoDao orangeDao;

    public GenericResponse callThirdPartyDemo(String applicationNo) throws MasterServiceApiException, URI.MalformedURIException {
        OrangeDemoResponse thirdPartyResponse = this.orangeDao.getApplicationFromThirdPartyDemo(applicationNo);
        GenericResponse response = new GenericResponse();
        response.setData(thirdPartyResponse);
        response.setStatus(ResultCode.SUCCESS);
        return response;
    }
}
