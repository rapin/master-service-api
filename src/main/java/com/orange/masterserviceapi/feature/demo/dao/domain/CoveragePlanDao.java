package com.orange.masterserviceapi.feature.demo.dao.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CoveragePlanDao {
    private String coverageName;
    private String coverageCode;
    List<CoverageDetailDao> coverageDetailDaoList;
}
