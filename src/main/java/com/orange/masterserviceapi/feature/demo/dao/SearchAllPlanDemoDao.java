package com.orange.masterserviceapi.feature.demo.dao;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanRepo;
import com.orange.masterserviceapi.feature.demo.dao.domain.CoverageDetailDao;
import com.orange.masterserviceapi.feature.demo.dao.domain.CoveragePlanDao;
import com.orange.masterserviceapi.feature.demo.dao.domain.TaProductDao;
import com.orange.masterserviceapi.feature.demo.dao.domain.TravelPlanModel;
import com.orange.masterserviceapi.feature.demo.service.domain.*;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
@Slf4j
public class SearchAllPlanDemoDao {
    @Autowired private MstTaPlanRepo mstTaPlanRepo;

    public TravelPlanResponse getAllPlanDemo() throws MasterServiceApiException {
        List<TravelPlanModel> travelPlanModelList = new ArrayList<TravelPlanModel>();
        travelPlanModelList = this.mstTaPlanRepo.findTravelPlan();
        if (travelPlanModelList.isEmpty())
            throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

        List<TravelProductResponse> productList = new ArrayList<>();
        List<CoveragePlanDao> coveragePlanList = new ArrayList<>();
        List<CoverageDetailDao> coverageDetailList = new ArrayList<>();

        TravelProductResponse productDao1 = new TravelProductResponse();
        CoveragePlanDao coverageDao1 = new CoveragePlanDao();
        CoverageDetailDao detailDao1 = new CoverageDetailDao();
        for(TravelPlanModel cache1 : travelPlanModelList) {
            TravelProductResponse productDao2 = new TravelProductResponse();
            productDao2.setProductName(cache1.getTravelPlanName());
            productDao2.setPrice(cache1.getPrice());

            CoveragePlanDao coverageDao2 = new CoveragePlanDao();
            coverageDao2.setCoverageCode(cache1.getCoveragePlanCode());
            coverageDao2.setCoverageName(cache1.getCoveragePlanName());

            CoverageDetailDao coverageDetail = new CoverageDetailDao();
            coverageDetail.setDetailCode(cache1.getCoveragePlanDetailGroup());
            coverageDetail.setDetailName(cache1.getCoveragePlanDetailName());
            coverageDetail.setCoverage(cache1.getCoveragePlanCode());
            coverageDetail.setPlanName(cache1.getTravelPlanName());

            if (StringUtils.isEmpty(productDao1.getProductName()))
                productList.add(productDao2);
            else {
                if (! productDao1.getProductName().equals(productDao2.getProductName())) {
                    productList.add(productDao2);
                }
            }
            productDao1 = productDao2;

            if (StringUtils.isEmpty(coverageDao1.getCoverageCode()))
                coveragePlanList.add(coverageDao2);
            else {
                if (! coverageDao1.getCoverageCode().equals(coverageDao2.getCoverageCode())) {
                    coveragePlanList.add(coverageDao2);
                }
            }
            coverageDao1 = coverageDao2;

            if (StringUtils.isEmpty(detailDao1.getDetailCode()))
                coverageDetailList.add(coverageDetail);
            else {
                if (! detailDao1.getDetailCode().equals(coverageDetail.getDetailCode())) {
                    coverageDetailList.add(coverageDetail);
                }
            }
            detailDao1 = coverageDetail;
        }

        List<CoveragePlanDao> groupCoveragePlanList = this.groupCoveragePlan(coveragePlanList);
        List<CoveragePlanDao> groupPlanMappingList = this.groupPlanMappingByCoveragePlanDetail(groupCoveragePlanList,travelPlanModelList);
        List<CoverageResponse> coverageResponseResult = this.groupCoverageDetail(groupPlanMappingList);

        TravelPlanResponse travelPlanResponse = new TravelPlanResponse();
        travelPlanResponse.setAllPlan(productList);
        travelPlanResponse.setAllCoverage(coverageResponseResult);
        return travelPlanResponse;
    }

    public List<CoveragePlanDao> groupCoveragePlan(List<CoveragePlanDao> coverageList) {
        ArrayList<CoveragePlanDao> cacheList = new ArrayList<>();
        cacheList.addAll(coverageList);
        Collections.sort(cacheList, new Comparator<CoveragePlanDao>() {
            @Override
            public int compare(CoveragePlanDao source, CoveragePlanDao compare)
            {
                return  source.getCoverageCode().compareTo(compare.getCoverageCode());
            }
        });
        List<CoveragePlanDao> coverageDaoList = new ArrayList<>();
        CoveragePlanDao duplicate = new CoveragePlanDao();
        boolean first = true;
        for (CoveragePlanDao coverage : cacheList){
            if (first) {
                CoveragePlanDao coverageDao = new CoveragePlanDao();
                coverageDao.setCoverageName(coverage.getCoverageName());
                coverageDao.setCoverageCode(coverage.getCoverageCode());
                coverageDaoList.add(coverageDao);
                first = false;
                duplicate = coverageDao;
            } else {
                if (! coverage.getCoverageCode().equals(duplicate.getCoverageCode())) {
                    CoveragePlanDao coverageDao = new CoveragePlanDao();
                    coverageDao.setCoverageName(coverage.getCoverageName());
                    coverageDao.setCoverageCode(coverage.getCoverageCode());
                    coverageDaoList.add(coverageDao);
                    duplicate = coverage;
                }
            }
        }
        return coverageDaoList;
    }

    public List<CoveragePlanDao> groupPlanMappingByCoveragePlanDetail(List<CoveragePlanDao> coverageList, List<TravelPlanModel> coverageListDetailList){
        ArrayList<TravelPlanModel> cacheList = new ArrayList<>();
        cacheList.addAll(coverageListDetailList);
        Collections.sort(cacheList, new Comparator<TravelPlanModel>() {
            @Override
            public int compare(TravelPlanModel source, TravelPlanModel compare)
            {
                return  source.getCoveragePlanCode().compareTo(compare.getCoveragePlanCode());
            }
        });
        List<CoveragePlanDao> resultList = new ArrayList<>();
        List<CoverageDetailDao> newList = new ArrayList<>();
        for(CoveragePlanDao coverage : coverageList){
            for(Iterator<TravelPlanModel> iterator = cacheList.iterator(); iterator.hasNext();) {
                TravelPlanModel planModel = iterator.next();
                if (coverage.getCoverageCode().equals(planModel.getCoveragePlanCode())) {
                    CoverageDetailDao detailDao = new CoverageDetailDao();
                    detailDao.setDetailCode(planModel.getCoveragePlanDetailGroup());
                    detailDao.setDetailName(planModel.getCoveragePlanDetailName());
                    detailDao.setCoverage(planModel.getCoverage());
                    detailDao.setPlanName(planModel.getTravelPlanName());
                    newList.add(detailDao);
                    iterator.remove();
                }
            }
            coverage.setCoverageDetailDaoList(newList);
            resultList.add(coverage);
            newList = new ArrayList<>();
        }
        return resultList;
    }

    public List<CoverageResponse> groupCoverageDetail(List<CoveragePlanDao> coverageDaoList){
        List<CoverageResponse> coverageResponseList = new ArrayList<>();

        for (CoveragePlanDao coverage : coverageDaoList){
            CoverageResponse coverageResponse = new CoverageResponse();
            coverageResponse.setCoverageCode(coverage.getCoverageCode());
            coverageResponse.setCoverageName(coverage.getCoverageName());

            List<CoverageDetailDao> coverageDetail = new ArrayList<>();
            coverageDetail = coverage.getCoverageDetailDaoList();
            Collections.sort(coverageDetail, new Comparator<CoverageDetailDao>() {
                @Override
                public int compare(CoverageDetailDao source, CoverageDetailDao compare)
                {
                    return  source.getDetailCode().compareTo(compare.getDetailCode());
                }
            });

            List<CoverageDetailResponse> coverageDetailResponseList = new ArrayList<>();
            List<ProductResponse> productResponseList = new ArrayList<>();

            CoverageDetailDao staging = coverageDetail.get(0);
            for (CoverageDetailDao detailDaoCache : coverageDetail){
                if(staging.getDetailCode().equals(detailDaoCache.getDetailCode())){
                    ProductResponse productResponse = new ProductResponse();
                    productResponse.setPlanName(detailDaoCache.getPlanName());
                    productResponse.setCoverage(detailDaoCache.getCoverage());
                    productResponseList.add(productResponse);
                }else {
                    CoverageDetailResponse coverageDetailResponse = new CoverageDetailResponse();
                    coverageDetailResponse.setCoverageDetailCode(staging.getDetailCode());
                    coverageDetailResponse.setCoverageDetailName(staging.getDetailName());
                    coverageDetailResponse.setProductResponseList(productResponseList);
                    coverageDetailResponseList.add(coverageDetailResponse);

                    staging = detailDaoCache;
                    productResponseList = new ArrayList<>();

                    ProductResponse productResponse = new ProductResponse();
                    productResponse.setPlanName(detailDaoCache.getPlanName());
                    productResponse.setCoverage(detailDaoCache.getCoverage());
                    productResponseList.add(productResponse);
                }
            }
            CoverageDetailResponse coverageDetailResponse = new CoverageDetailResponse();
            coverageDetailResponse.setCoverageDetailCode(staging.getDetailCode());
            coverageDetailResponse.setCoverageDetailName(staging.getDetailName());
            coverageDetailResponse.setProductResponseList(productResponseList);
            coverageDetailResponseList.add(coverageDetailResponse);

            coverageResponse.setCoverageDetailResponseList(coverageDetailResponseList);
            coverageResponseList.add(coverageResponse);
        }
        return coverageResponseList;
    }
}
