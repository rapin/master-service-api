package com.orange.masterserviceapi.feature.demo.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown =true)
public class OrangeDemoResponse {
    @JsonProperty("application_number")
    private String applicationNumber;

    @JsonProperty("issue_date")
    private Date issueDate;

    @JsonProperty("plan_code")
    private String planCode;

    @JsonProperty("application_type")
    private String applicationType;
}
