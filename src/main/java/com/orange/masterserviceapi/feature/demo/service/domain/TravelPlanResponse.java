package com.orange.masterserviceapi.feature.demo.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TravelPlanResponse {
    @JsonProperty("all_plan")
    private List<TravelProductResponse> allPlan;

    @JsonProperty("all_coverage")
    private List<CoverageResponse> allCoverage;
}
