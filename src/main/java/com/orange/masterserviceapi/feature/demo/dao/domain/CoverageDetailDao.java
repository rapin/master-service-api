package com.orange.masterserviceapi.feature.demo.dao.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CoverageDetailDao {
    private String detailCode;
    private String detailName;
    private String planName;
    private String coverage;
    private List<TaProductDao> productDaoList;
}
