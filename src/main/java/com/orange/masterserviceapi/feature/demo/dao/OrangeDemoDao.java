package com.orange.masterserviceapi.feature.demo.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.masterserviceapi.common.config.CustomHandleInterceptor;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.demo.dao.domain.OrangeDemoResponse;
import com.sun.org.apache.xml.internal.utils.URI;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Repository
public class OrangeDemoDao {
    @Value("${third.party.uris}")
    private String uris;
    @Value("${third.party.api.endpoint}")
    private String endpoint;

    private RestTemplate orangeRouterRestTemplate;

    public OrangeDemoDao(RestTemplate orangeRouterRestTemplate) {
        this.orangeRouterRestTemplate = orangeRouterRestTemplate;
    }

    public OrangeDemoResponse getApplicationFromThirdPartyDemo(String applicationNo) throws MasterServiceApiException, URI.MalformedURIException {
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.set(CustomHandleInterceptor.CORRELATION_ID, MDC.get(CustomHandleInterceptor.CORRELATION_ID));
        httpHeaders.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        String apiEndpoint = uris.concat(endpoint.concat(applicationNo));
        final String baseUrl = apiEndpoint;
        URI uri = null;
        uri = new URI(baseUrl);
        log.info("## Pre send request : [{}]",uri);
        HttpEntity<OrangeDemoResponse> requestEntity = new HttpEntity<>(null, httpHeaders);
        try {
            ResponseEntity<GenericResponse> result = orangeRouterRestTemplate.exchange(uri.toString(),
                    HttpMethod.GET,
                    requestEntity,
                    GenericResponse.class);
            ObjectMapper objectMapper = new ObjectMapper();
            OrangeDemoResponse response = objectMapper.convertValue(result.getBody().getData(), OrangeDemoResponse.class);
            log.info("## ResponseEntity from third party :{{}}", response.getApplicationNumber());
            return response;
        }catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND)
                throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);
            throw new MasterServiceApiException(ResultCode.SYSTEM_ERROR);
        }
    }
}
