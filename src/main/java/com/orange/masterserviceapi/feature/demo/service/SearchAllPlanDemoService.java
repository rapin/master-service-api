package com.orange.masterserviceapi.feature.demo.service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.feature.demo.dao.SearchAllPlanDemoDao;
import com.orange.masterserviceapi.feature.demo.service.domain.TravelPlanResponse;
import com.orange.masterserviceapi.feature.demo.service.domain.TravelProductResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SearchAllPlanDemoService {

    @Autowired
    private SearchAllPlanDemoDao searchAllPlanDemoDao;

    public GenericResponse getAllTaPlanDemo(int dateAmount) throws MasterServiceApiException {
        long startTime = System.currentTimeMillis();
        if(dateAmount <= 0)
            throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

        TravelPlanResponse travelPlanResponse = this.searchAllPlanDemoDao.getAllPlanDemo();
        List<TravelProductResponse> taProduct = this.calculatePriceByDateAmount(travelPlanResponse.getAllPlan(),dateAmount);
        travelPlanResponse.setAllPlan(taProduct);
        GenericResponse response = new GenericResponse();
        response.setStatus(ResultCode.SUCCESS);
        response.setData(travelPlanResponse);
        long finishTime = System.currentTimeMillis();
        log.info("## Total process time : [{} milliseconds]",finishTime-startTime);
        return response;
    }

    public List<TravelProductResponse> calculatePriceByDateAmount(List<TravelProductResponse> productDaoList , int dateAmount){
        List<TravelProductResponse> responsesList = new ArrayList<>();
        for(TravelProductResponse productDaoCache : productDaoList){
            TravelProductResponse  travelProductResponse = new TravelProductResponse();
            travelProductResponse.setProductName(productDaoCache.getProductName());
            travelProductResponse.setPrice(String.valueOf(Double.parseDouble(productDaoCache.getPrice()) * dateAmount));
            responsesList.add(travelProductResponse);
        }
        return responsesList;
    }
}
