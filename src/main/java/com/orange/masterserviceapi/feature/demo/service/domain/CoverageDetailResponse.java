package com.orange.masterserviceapi.feature.demo.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.orange.masterserviceapi.feature.demo.service.CallThirdPartyDemoService;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CoverageDetailResponse {
    @JsonProperty("coverage_detail_code")
    private String coverageDetailCode;

    @JsonProperty("coverage_detail_name")
    private String coverageDetailName;

    @JsonProperty("plan")
    private List<ProductResponse> productResponseList;
}
