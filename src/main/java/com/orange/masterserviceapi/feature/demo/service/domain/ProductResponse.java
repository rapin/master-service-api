package com.orange.masterserviceapi.feature.demo.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductResponse {
    @JsonProperty("plan_name")
    private String planName;
    private String coverage;
}
