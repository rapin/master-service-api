package com.orange.masterserviceapi.feature.demo.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelProductResponse {
    @JsonProperty("product_name")
    private String productName;
    private String price;
}
