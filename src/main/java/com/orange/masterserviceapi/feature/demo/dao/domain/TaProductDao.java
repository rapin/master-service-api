package com.orange.masterserviceapi.feature.demo.dao.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaProductDao {
    private String productName;
    private String price;
}
