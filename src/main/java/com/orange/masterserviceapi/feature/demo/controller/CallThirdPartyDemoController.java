package com.orange.masterserviceapi.feature.demo.controller;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.feature.demo.service.CallThirdPartyDemoService;
import com.orange.masterserviceapi.feature.demo.service.SearchAllPlanDemoService;
import com.sun.org.apache.xml.internal.utils.URI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@Api(tags = "demo", hidden = true)
@RequestMapping(path = "/v1/demo")
public class CallThirdPartyDemoController {
    @Autowired private CallThirdPartyDemoService callThirdPartyDemoService;
    @Autowired private SearchAllPlanDemoService searchAllPlanDemoService;

    @ApiOperation(value = "Demo", notes = "Demo call third party via custom rest template")
    @GetMapping("/{app_no}")
    public ResponseEntity<GenericResponse> callThirdPartyDemo(
            @RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
            @PathVariable(name = "app_no", required = true) String appNo) throws MasterServiceApiException, URI.MalformedURIException {
        GenericResponse response = this.callThirdPartyDemoService.callThirdPartyDemo(appNo);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
    }

    @ApiOperation(value = "Demo", notes = "Demo get all plan demo")
    @GetMapping("/inquiry/{date_amount}")
    public ResponseEntity<GenericResponse> getAllPlanDemo(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
                                                          @PathVariable(value = "date_amount" ,required = true) int dateAmount) throws MasterServiceApiException {
        GenericResponse response = this.searchAllPlanDemoService.getAllTaPlanDemo(dateAmount);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return responseEntity;
    }
}
