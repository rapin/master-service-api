package com.orange.masterserviceapi.feature.appta.service;

import java.util.Optional;
import java.util.UUID;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTravelPlantRequestController;
import com.orange.masterserviceapi.feature.appta.controller.domain.TaPlanWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaPlan;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanRepo;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MasterTaPlanService {
	private static final String FLAG_NO = "N";

	@Autowired
	private MstTaPlanRepo mstTaPlanRepo;

	public GenericResponse insertMstTravelPlan(MstTravelPlantRequestController travelPlantRequest)
			throws MasterServiceApiException {
		if (StringUtils.isEmpty(travelPlantRequest.getPlanCode()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(travelPlantRequest.getPlanName()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		GenericResponse response = new GenericResponse();
		MstTaPlan mstTaPlan = new MstTaPlan();
		BeanUtils.copyProperties(travelPlantRequest, mstTaPlan);
		mstTaPlan.setRowId(UUID.randomUUID().toString().replace("-", ""));
		this.mstTaPlanRepo.saveAndFlush(mstTaPlan);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse updateMstTravelPlan(MstTravelPlantRequestController travelPlantRequest, String id)
			throws MasterServiceApiException {
		;
		Optional<MstTaPlan> mstTaPlan = this.mstTaPlanRepo.findById(id);
		if (!mstTaPlan.isPresent())
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
		if (StringUtils.isNotEmpty(travelPlantRequest.getPlanName()))
			mstTaPlan.get().setPlanName(travelPlantRequest.getPlanName());

		if (travelPlantRequest.getPlanPrice() != null)
			mstTaPlan.get().setPlanPrice(travelPlantRequest.getPlanPrice());

		mstTaPlan.get().setIsActive(travelPlantRequest.getIsActive());
		this.mstTaPlanRepo.saveAndFlush(mstTaPlan.get());
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse deleteMstTravelPlan(String id) throws MasterServiceApiException {
		Optional<MstTaPlan> mstTaPlan = this.mstTaPlanRepo.findById(id);
		if (!mstTaPlan.isPresent())
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
		mstTaPlan.get().setIsActive(FLAG_NO);
		this.mstTaPlanRepo.saveAndFlush(mstTaPlan.get());
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse getMstTravelPlan(String planCode) throws MasterServiceApiException {
		MstTaPlan mstTaPlan = this.mstTaPlanRepo.findByPlanCode(planCode);
		if (mstTaPlan == null)
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
		TaPlanWrapper wrapper = new TaPlanWrapper();
		BeanUtils.copyProperties(mstTaPlan, wrapper);
		response.setData(wrapper);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}
}
