package com.orange.masterserviceapi.feature.appta.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MstTravelPlantRequestController {
    @JsonProperty("plan_code")
    private String planCode;

    @JsonProperty("plan_name")
    private String planName;

    @JsonProperty("plan_price")
    private Double planPrice;
    
    @JsonProperty("is_active")
    private String isActive;
}
