package com.orange.masterserviceapi.feature.appta.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MstTaCoveragePlanDetailRequestController {

	@JsonProperty("coverage_plan_detail_code")
	private String coveragePlanDetailCode;

	@JsonProperty("coverage_plan_detail_name")
	private String coveragePlanDetailName;

	@JsonProperty("coverage_plan_group")
	private String coveragePlanGroup;

	@JsonProperty("is_active")
	private String isActive;
}
