package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaPlanMapCoverageRequestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.feature.appta.service.MasterTaPlanMapCoverageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Api(tags = "ta-map-coverage", hidden = true)
@RequestMapping(path = "/v1/ta-map-coverage")
public class MasterTaPlanMapCoverageController {
	@Autowired
	private MasterTaPlanMapCoverageService masterTaPlanMapCoverageService;

	@ApiOperation(value = "Insert Master Travel Plan Mapping Coverage")
	@PostMapping
	public ResponseEntity<GenericResponse> insertTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@RequestBody MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest)
			throws MasterServiceApiException {
		GenericResponse response = this.masterTaPlanMapCoverageService
				.insertMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest);
		ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(response);
		return responseEntity;
	}

	@ApiOperation(value = "Update Master Travel Plan Mapping Coverage")
	@PutMapping(value = "/{id}")
	public ResponseEntity<GenericResponse> updateTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@PathVariable("id") String id,
			@RequestBody MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest)
			throws MasterServiceApiException {
		return ResponseEntity.status(HttpStatus.OK).body(
				this.masterTaPlanMapCoverageService.updateMstTaPlanMapCoverage(mstTaPlanMapCoverageRequest, id));
	}

	@ApiOperation(value = "Delete Master Travel Plan Mapping Coverage")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<GenericResponse> deleteTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@PathVariable("id") String id) throws MasterServiceApiException {
		return ResponseEntity.status(HttpStatus.OK)
				.body(this.masterTaPlanMapCoverageService.deleteMstTaPlanMapCoverage(id));
	}

	@ApiOperation(value = "Search Master Travel Plan Mapping Coverage")
	@GetMapping(value = "/{plan_code}")
	public ResponseEntity<GenericResponse> searchTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@PathVariable("plan_code") String planCode) throws MasterServiceApiException {
		return ResponseEntity.status(HttpStatus.OK)
				.body(this.masterTaPlanMapCoverageService.getMstTaPlanMapCoverage(planCode));
	}
}
