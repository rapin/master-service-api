package com.orange.masterserviceapi.feature.appta.service.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelCoveragePlanDetail {
	@JsonProperty("travel_coverage_plan_detail_code")
	private String travelCoveragePlanDetailCode;

	@JsonProperty("travel_coverage_plan_detail_description")
	private String travelCoveragePlanDetailDescription;

	@JsonProperty("lst_travel_plan")
	private List<TravelPlan> lstTravelPlan;
}
