package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.feature.appta.service.TravelPlanService;
import com.sun.org.apache.xml.internal.utils.URI;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "journey", hidden = true)
@RequestMapping(path = "/v1/journey")
public class TravelPlanController {

	@Autowired private TravelPlanService travelPlanService;

	@ApiOperation(value = "Get travel plan", notes = "Demo get all travel plan by criteria")
	@GetMapping(value = "/{date_amount}")
	public ResponseEntity<GenericResponse> getAllPlan(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
													  @PathVariable(value = "date_amount" ,required = true) int dateAmount) throws Exception {
		GenericResponse response = this.travelPlanService.getTravelPlan(dateAmount);
		ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		return responseEntity;
	}
}
