package com.orange.masterserviceapi.feature.appta.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.orange.masterserviceapi.common.exception.MasterServiceApiBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlan;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlanDetail;
import com.orange.masterserviceapi.datasource.entities.MstTaPlan;
import com.orange.masterserviceapi.datasource.entities.MstTaPlanMapCoverage;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanDetailRepo;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanRepo;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanMapCoverageRepo;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanRepo;
import com.orange.masterserviceapi.feature.appta.service.domain.InquiryTravelPlanResponse;
import com.orange.masterserviceapi.feature.appta.service.domain.TravelCoveragePlan;
import com.orange.masterserviceapi.feature.appta.service.domain.TravelCoveragePlanDetail;
import com.orange.masterserviceapi.feature.appta.service.domain.TravelPlan;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TravelPlanService {
	@Autowired private MstTaCoveragePlanDetailRepo mstTaCoveragePlanDetailRepo;
	@Autowired private MstTaCoveragePlanRepo mstTaCoveragePlanRepo;
	@Autowired private MstTaPlanRepo mstTaPlanRepo;
	@Autowired private MstTaPlanMapCoverageRepo mstTaPlanMapCoverageRepo;
	private static final String APPLICATION_TYPE = "TA";

    public GenericResponse getTravelPlan(int dateAmount) throws MasterServiceApiException {
        long startTime = System.currentTimeMillis();
        if (dateAmount <= 0)
            throw new MasterServiceApiBusinessException(ResultCode.INVALID_BUSINESS,"Date amount must be greater than 0.");

        List<MstTaPlan> listTaPlan = mstTaPlanRepo.findAllByOrderByPlanCodeAsc();
        if (listTaPlan.isEmpty())
            throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

        GenericResponse response = new GenericResponse();
        List<MstTaPlanMapCoverage> listTaPlanMapCoverage = mstTaPlanMapCoverageRepo.findAllByOrderByPlanCodeAsc();
        List<MstTaCoveragePlanDetail> listCoveragePlanDetail = mstTaCoveragePlanDetailRepo
                .findAllByOrderByCoveragePlanGroupAscCoveragePlanDetailCodeAsc();
        List<MstTaCoveragePlan> listCoveragePlan = mstTaCoveragePlanRepo.findAllByOrderByCoverageCodeAsc();
        listTaPlan.stream().forEach(item -> item.setPlanPrice(item.getPlanPrice() * dateAmount));
        response.setData(
                mappingCoveragePlan(listTaPlan, listCoveragePlan, listCoveragePlanDetail, listTaPlanMapCoverage));
        response.setStatus(ResultCode.SUCCESS);

        long finishTime = System.currentTimeMillis();
        log.info("## Total process time : [{} milliseconds]",finishTime-startTime);
        return response;
    }

    private InquiryTravelPlanResponse mappingCoveragePlan(List<MstTaPlan> listTaPlan,
                                                          List<MstTaCoveragePlan> listCoveragePlan, List<MstTaCoveragePlanDetail> listCoveragePlanDetail,
                                                          List<MstTaPlanMapCoverage> listTaPlanMapConverage) {
        // Mapping
        List<TravelCoveragePlan> mapGroup = new ArrayList<>();
        for (MstTaCoveragePlan group : listCoveragePlan) {
            TravelCoveragePlan item = new TravelCoveragePlan();
            List<TravelCoveragePlanDetail> lstTravelCoveragePlanDetail = new ArrayList<>();
            item.setTravelCoverageGroupId(group.getCoverageCode());
            item.setTravelCoverageName(group.getCoverageName());
            for (MstTaCoveragePlanDetail detail : listCoveragePlanDetail) {
                List<TravelPlan> lstTravelPlan = new ArrayList<>();
                if (group.getCoverageCode().equals(detail.getCoveragePlanGroup())) { // SameGroup
                    TravelCoveragePlanDetail travelCoveragePlanDetail = new TravelCoveragePlanDetail();
                    travelCoveragePlanDetail.setTravelCoveragePlanDetailCode(detail.getCoveragePlanDetailCode());
                    travelCoveragePlanDetail.setTravelCoveragePlanDetailDescription(detail.getCoveragePlanDetailName());
                    for (MstTaPlanMapCoverage map : listTaPlanMapConverage) {
                        if (detail.getCoveragePlanDetailCode().equals(map.getCoveragePlanDetailCode())) {
                            TravelPlan travelPlan = new TravelPlan();
                            travelPlan.setCoveragePrice(String.valueOf(map.getPrice()));
                            travelPlan.setPlanName(map.getMstTaPlan().getPlanName());
                            lstTravelPlan.add(travelPlan);
                        }
                    }
                    for (MstTaPlan taPlan : listTaPlan) {
                        TravelPlan found = lstTravelPlan.stream()
                                .filter(e -> e.getPlanName().equals(taPlan.getPlanName())).findAny().orElse(null);
                        if (found == null) {
                            TravelPlan travelPlan = new TravelPlan();
                            travelPlan.setCoveragePrice(null);
                            travelPlan.setPlanName(taPlan.getPlanName());
                            lstTravelPlan.add(travelPlan);
                        }
                    }
                    travelCoveragePlanDetail.setLstTravelPlan(lstTravelPlan);
                    lstTravelCoveragePlanDetail.add(travelCoveragePlanDetail);
                }
            }
            item.setLstTravelCoveragePlanDetail(lstTravelCoveragePlanDetail);
            mapGroup.add(item);

        }
        List<TravelPlan> lstTravelPlanDetail = new ArrayList<>();
        for (MstTaPlan taPlan : listTaPlan) {
            TravelPlan item = new TravelPlan();
            item.setPriceFromCalculate(String.valueOf(taPlan.getPlanPrice()));
            item.setPlanName(taPlan.getPlanName());
            lstTravelPlanDetail.add(item);
        }

        // Sorting
        for (TravelCoveragePlan sorting : mapGroup) {
            for (TravelCoveragePlanDetail lstTravelCoveragePlanDetail : sorting.getLstTravelCoveragePlanDetail()) {
                sortingTravelPlan(lstTravelCoveragePlanDetail.getLstTravelPlan());
            }
        }
        sortingTravelPlan(lstTravelPlanDetail);

        InquiryTravelPlanResponse inquiryTravelPlanResponse = new InquiryTravelPlanResponse();
        inquiryTravelPlanResponse.setLstTravelPlanDetail(lstTravelPlanDetail);
        inquiryTravelPlanResponse.setLstTravelCoveragePlan(mapGroup);
        return inquiryTravelPlanResponse;
    }

    private void sortingTravelPlan(List<TravelPlan> list) {
        Collections.sort(list, new Comparator<TravelPlan>() {
            @Override
            public int compare(TravelPlan o1, TravelPlan o2) {
                return o1.getPlanName().compareTo(o2.getPlanName());
            }
        });
    }
}

