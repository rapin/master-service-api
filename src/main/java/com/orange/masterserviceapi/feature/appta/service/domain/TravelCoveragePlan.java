package com.orange.masterserviceapi.feature.appta.service.domain;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelCoveragePlan {
    @JsonProperty("travel_coverage_group_id")
    private String travelCoverageGroupId;
    
    @JsonProperty("travel_coverage_name")
    private String travelCoverageName;
    
    @JsonProperty("lst_travel_coverage_plan_detail")
    private List<TravelCoveragePlanDetail> lstTravelCoveragePlanDetail;
    


}
