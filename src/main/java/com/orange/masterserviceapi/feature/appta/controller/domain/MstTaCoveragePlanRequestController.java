package com.orange.masterserviceapi.feature.appta.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MstTaCoveragePlanRequestController {

	@JsonProperty("coverage_code")
	private String coverageCode;

	@JsonProperty("coverage_name")
	private String coverageName;

	@JsonProperty("is_active")
	private String isActive;
}
