package com.orange.masterserviceapi.feature.appta.service.domain;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class MstTaPlanResponseController {

	private double planPrice;

    private String taPlanName;

    private String planDetailCode;
    
}
