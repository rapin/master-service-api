package com.orange.masterserviceapi.feature.appta.service.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelPlan {
    @JsonProperty("plan_name")
    private String planName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("price_from_calculate")
    private String priceFromCalculate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("coverage_price")
    private String coveragePrice;

}
