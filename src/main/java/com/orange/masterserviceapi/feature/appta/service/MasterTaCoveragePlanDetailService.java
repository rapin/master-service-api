package com.orange.masterserviceapi.feature.appta.service;

import java.util.UUID;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanDetailRequestController;
import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanDetailResponseController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlanDetail;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanDetailRepo;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MasterTaCoveragePlanDetailService {
	private static final String FLAG_YES = "Y";
	private static final String FLAG_NO = "N";

	@Autowired
	private MstTaCoveragePlanDetailRepo mstTaCoveragePlanDetailRepo;
	
	public GenericResponse insertMstTaCoveragePlanDetail(MstTaCoveragePlanDetailRequestController mstTaCoveragePlanDetailRequest) throws MasterServiceApiException {

		if (StringUtils.isEmpty(mstTaCoveragePlanDetailRequest.getCoveragePlanDetailCode()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaCoveragePlanDetailRequest.getCoveragePlanDetailName()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaCoveragePlanDetailRequest.getCoveragePlanGroup()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		GenericResponse response = new GenericResponse();
		MstTaCoveragePlanDetail mstTaCoveragePlanDetail = new MstTaCoveragePlanDetail();
		String rowId = UUID.randomUUID().toString();
		rowId = rowId.replace("-", "");
		BeanUtils.copyProperties(mstTaCoveragePlanDetailRequest, mstTaCoveragePlanDetail);
		mstTaCoveragePlanDetail.setRowId(rowId);
		mstTaCoveragePlanDetail.setIsActive(FLAG_YES);
		this.mstTaCoveragePlanDetailRepo.saveAndFlush(mstTaCoveragePlanDetail);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}
	
	public GenericResponse updateMstTaCoveragePlanDetail(MstTaCoveragePlanDetailRequestController mstTaCoveragePlanDetailRequest) throws MasterServiceApiException{
		if(StringUtils.isEmpty(mstTaCoveragePlanDetailRequest.getCoveragePlanDetailName()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);
		
		if(StringUtils.isEmpty(mstTaCoveragePlanDetailRequest.getCoveragePlanGroup()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);
		
		if(StringUtils.isEmpty(mstTaCoveragePlanDetailRequest.getIsActive()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);
       
		GenericResponse response = new GenericResponse();
		MstTaCoveragePlanDetail mstTaCoveragePlanDetail = this.mstTaCoveragePlanDetailRepo.findByCoveragePlanDetailCode(mstTaCoveragePlanDetailRequest.getCoveragePlanDetailCode());
        if(mstTaCoveragePlanDetail == null)
        	throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);
    	BeanUtils.copyProperties(mstTaCoveragePlanDetailRequest, mstTaCoveragePlanDetail);
        this.mstTaCoveragePlanDetailRepo.saveAndFlush(mstTaCoveragePlanDetail);
        response.setStatus(ResultCode.SUCCESS);
        return response;
    }
	
	public GenericResponse deleteMstTaCoveragePlanDetail(String coverageDetailCode) throws MasterServiceApiException{
        MstTaCoveragePlanDetail mstTaCoveragePlanDetail = this.mstTaCoveragePlanDetailRepo.findByCoveragePlanDetailCode(coverageDetailCode);
        if(mstTaCoveragePlanDetail == null)
        	throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
        mstTaCoveragePlanDetail.setIsActive(FLAG_NO);
        this.mstTaCoveragePlanDetailRepo.saveAndFlush(mstTaCoveragePlanDetail);
        response.setStatus(ResultCode.SUCCESS);
        return response;
    }
	
	public GenericResponse searchMstTaCoveragePlanDetail(String coverageDetailCode) throws MasterServiceApiException{
		MstTaCoveragePlanDetail mstTaCoveragePlanDetail = this.mstTaCoveragePlanDetailRepo.findByCoveragePlanDetailCode(coverageDetailCode);
        if(mstTaCoveragePlanDetail == null)
        	throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
        MstTaCoveragePlanDetailResponseController mstTaCoveragePlanDetailResponse = new MstTaCoveragePlanDetailResponseController();
		BeanUtils.copyProperties(mstTaCoveragePlanDetail, mstTaCoveragePlanDetailResponse);
		response.setStatus(ResultCode.SUCCESS);
		response.setData(mstTaCoveragePlanDetailResponse);
		return response;
	}

}
