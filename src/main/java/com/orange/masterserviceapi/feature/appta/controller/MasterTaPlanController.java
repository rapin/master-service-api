package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTravelPlantRequestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.feature.appta.service.MasterTaPlanService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Api(tags = "ta-plans", hidden = true)
@RequestMapping(path = "/v1/ta-plans")
public class MasterTaPlanController {
	@Autowired
	private MasterTaPlanService masterTaPlanService;

	@ApiOperation(value = "Insert Master Travel Plan")
	@PostMapping
	public ResponseEntity<GenericResponse> insertTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@RequestBody MstTravelPlantRequestController mstTravelPlantRequest) throws MasterServiceApiException {
		GenericResponse response = this.masterTaPlanService.insertMstTravelPlan(mstTravelPlantRequest);
		ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(response);
		return responseEntity;
	}

	@ApiOperation(value = "Update Master Travel Plan")
	@PutMapping(value = "/{id}")
	public ResponseEntity<GenericResponse> updateTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@PathVariable("id") String id, @RequestBody MstTravelPlantRequestController mstTravelPlantRequest)
			throws MasterServiceApiException {
		return ResponseEntity.status(HttpStatus.OK).body(this.masterTaPlanService.updateMstTravelPlan(mstTravelPlantRequest, id));
	}

	@ApiOperation(value = "Delete Master Travel Plan")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<GenericResponse> deleteTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@PathVariable("id") String id) throws MasterServiceApiException {
		return ResponseEntity.status(HttpStatus.OK).body(this.masterTaPlanService.deleteMstTravelPlan(id));
	}
	
	@ApiOperation(value = "Search Master Travel Plan")
	@GetMapping(value = "/{plan_code}")
	public ResponseEntity<GenericResponse> searchTravelPlan(
			@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			@PathVariable("planCode") String planCode) throws MasterServiceApiException {
		return ResponseEntity.status(HttpStatus.OK).body(this.masterTaPlanService.getMstTravelPlan(planCode));
	}
}
