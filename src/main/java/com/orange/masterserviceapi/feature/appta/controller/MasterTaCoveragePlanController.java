package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanRequestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.feature.appta.service.MasterTaCoveragePlanService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Api(tags = "ta-coverage-plans", hidden = true)
@RequestMapping(path = "/v1/ta-coverage-plans")
public class MasterTaCoveragePlanController {

	@Autowired
	private MasterTaCoveragePlanService masterTaCoveragePlanService;

	@ApiOperation(value = "Insert Master Travel Coverage Plan")
	@PostMapping
	public ResponseEntity<GenericResponse> insertMstTaCoveragePlan(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
													@RequestBody MstTaCoveragePlanRequestController mstTaCoveragePlanRequest) throws MasterServiceApiException {
		GenericResponse response = this.masterTaCoveragePlanService.insertMstTaCoveragePlan(mstTaCoveragePlanRequest);
		ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(response);
		return responseEntity;
	}

	@ApiOperation(value = "Update Master Travel Coverage Plan")
	@PutMapping
	public ResponseEntity<GenericResponse> updateMstTaCoveragePlan(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			 												   	@RequestBody MstTaCoveragePlanRequestController mstTaCoveragePlanRequest) throws MasterServiceApiException{
        return   ResponseEntity.status(HttpStatus.OK).body(this.masterTaCoveragePlanService.updateMstTaCoveragePlan(mstTaCoveragePlanRequest));
	}
	
	@ApiOperation(value = "Delete Master Travel Coverage Plan")
	@DeleteMapping("/{coverage_code}")
	public ResponseEntity<GenericResponse> deleteMstTaCoveragePlan(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
															@PathVariable("coverage_code") String coverageCode) throws MasterServiceApiException{
	
	    GenericResponse response = this.masterTaCoveragePlanService.deleteMstTaCoveragePlan(coverageCode);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
	}
	
	@ApiOperation(value = "Search Master Travel Coverage Plan")
    @GetMapping("/{coverage_code}")
    public ResponseEntity<GenericResponse> searchMstTaCoveragePlan(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
    														@PathVariable("coverage_code") String coverageCode) throws MasterServiceApiException {
    	GenericResponse response = this.masterTaCoveragePlanService.searchMstTaCoveragePlan(coverageCode);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
    }

}
