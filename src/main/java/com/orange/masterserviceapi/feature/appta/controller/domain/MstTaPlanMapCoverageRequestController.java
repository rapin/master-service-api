package com.orange.masterserviceapi.feature.appta.controller.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MstTaPlanMapCoverageRequestController {
	@JsonProperty("plan_code")
	private String planCode;
	
	@JsonProperty("coverage_plan_code")
	private String coveragePlanCode;
	
	@JsonProperty("coverage_plan_detail_code")
	private String coveragePlanDetailCode;
	
	private Double price;

	@JsonProperty("is_active")
	private String isActive;
}
