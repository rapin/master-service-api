package com.orange.masterserviceapi.feature.appta.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaPlanMapCoverageRequestController;
import com.orange.masterserviceapi.feature.appta.controller.domain.TaPlanMapCoverageWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaPlanMapCoverage;
import com.orange.masterserviceapi.datasource.repo.MstTaPlanMapCoverageRepo;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MasterTaPlanMapCoverageService {
	private static final String FLAG_NO = "N";
	@Autowired
	private MstTaPlanMapCoverageRepo mstTaPlanMapCoverageRepo;

	public GenericResponse insertMstTaPlanMapCoverage(MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest)
			throws MasterServiceApiException {
		if (StringUtils.isEmpty(mstTaPlanMapCoverageRequest.getPlanCode()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaPlanMapCoverageRequest.getCoveragePlanCode()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaPlanMapCoverageRequest.getCoveragePlanDetailCode()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		GenericResponse response = new GenericResponse();
		MstTaPlanMapCoverage mstTaPlanMapCoverage = new MstTaPlanMapCoverage();
		String rowId = UUID.randomUUID().toString();
		rowId = rowId.replace("-", "");
		BeanUtils.copyProperties(mstTaPlanMapCoverageRequest, mstTaPlanMapCoverage);
		mstTaPlanMapCoverage.setRowId(rowId);
		this.mstTaPlanMapCoverageRepo.saveAndFlush(mstTaPlanMapCoverage);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse updateMstTaPlanMapCoverage(MstTaPlanMapCoverageRequestController mstTaPlanMapCoverageRequest,
			String id) throws MasterServiceApiException {
		Optional<MstTaPlanMapCoverage> mstTaPlanMapCoverage = this.mstTaPlanMapCoverageRepo.findById(id);
		if (!mstTaPlanMapCoverage.isPresent())
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		if (mstTaPlanMapCoverageRequest.getPrice() != null)
			mstTaPlanMapCoverage.get().setPrice(mstTaPlanMapCoverageRequest.getPrice());

		GenericResponse response = new GenericResponse();
		mstTaPlanMapCoverage.get().setIsActive(mstTaPlanMapCoverageRequest.getIsActive());
		this.mstTaPlanMapCoverageRepo.saveAndFlush(mstTaPlanMapCoverage.get());
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse deleteMstTaPlanMapCoverage(String rowId) throws MasterServiceApiException {
		Optional<MstTaPlanMapCoverage> mstTaPlanMapCoverage = this.mstTaPlanMapCoverageRepo.findById(rowId);
		if (!mstTaPlanMapCoverage.isPresent())
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
		mstTaPlanMapCoverage.get().setIsActive(FLAG_NO);
		this.mstTaPlanMapCoverageRepo.saveAndFlush(mstTaPlanMapCoverage.get());
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse getMstTaPlanMapCoverage(String planCode) throws MasterServiceApiException {
		GenericResponse response = new GenericResponse();
		List<MstTaPlanMapCoverage> mstTaPlan = this.mstTaPlanMapCoverageRepo.findByPlanCode(planCode);
		if (mstTaPlan == null)
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);
		List<TaPlanMapCoverageWrapper> wrapper = new ArrayList<TaPlanMapCoverageWrapper>();
		for (MstTaPlanMapCoverage source : mstTaPlan) {
			TaPlanMapCoverageWrapper taWrapper = new TaPlanMapCoverageWrapper();
			BeanUtils.copyProperties(source, taWrapper);
			wrapper.add(taWrapper);
		}
		response.setData(wrapper);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}
}
