package com.orange.masterserviceapi.feature.appta.service;

import java.util.UUID;
import java.util.regex.Pattern;

import com.orange.masterserviceapi.common.utils.ConstantUtils;
import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanRequestController;
import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanResponseController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;
import com.orange.masterserviceapi.datasource.entities.MstTaCoveragePlan;
import com.orange.masterserviceapi.datasource.repo.MstTaCoveragePlanRepo;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MasterTaCoveragePlanService {
	@Autowired
	private MstTaCoveragePlanRepo mstTaCoveragePlanRepo;

	public GenericResponse insertMstTaCoveragePlan(MstTaCoveragePlanRequestController mstTaCoveragePlanRequest)
			throws MasterServiceApiException {

		if (StringUtils.isEmpty(mstTaCoveragePlanRequest.getCoverageCode()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaCoveragePlanRequest.getCoverageName()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaCoveragePlanRequest.getIsActive()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (!Pattern.matches(ConstantUtils.Y_FLAG.concat("|").concat(ConstantUtils.N_FLAG), mstTaCoveragePlanRequest.getIsActive()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		GenericResponse response = new GenericResponse();
		MstTaCoveragePlan mstTaCoveragePlan = new MstTaCoveragePlan();
		String rowId = UUID.randomUUID().toString();
		rowId = rowId.replace("-", "");
		BeanUtils.copyProperties(mstTaCoveragePlanRequest, mstTaCoveragePlan);
		mstTaCoveragePlan.setRowId(rowId);
		this.mstTaCoveragePlanRepo.saveAndFlush(mstTaCoveragePlan);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse updateMstTaCoveragePlan(MstTaCoveragePlanRequestController mstTaCoveragePlanRequest)
			throws MasterServiceApiException {
		if (StringUtils.isEmpty(mstTaCoveragePlanRequest.getCoverageName()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (StringUtils.isEmpty(mstTaCoveragePlanRequest.getIsActive()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		if (!Pattern.matches(ConstantUtils.Y_FLAG.concat("|").concat(ConstantUtils.N_FLAG), mstTaCoveragePlanRequest.getIsActive()))
			throw new MasterServiceApiDataValidateException(ResultCode.INVALID_PARAM);

		GenericResponse response = new GenericResponse();
		MstTaCoveragePlan mstTaCoveragePlan = this.mstTaCoveragePlanRepo
				.findByCoverageCode(mstTaCoveragePlanRequest.getCoverageCode());
		if (mstTaCoveragePlan == null)
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		mstTaCoveragePlan.setCoverageName(mstTaCoveragePlanRequest.getCoverageName());
		mstTaCoveragePlan.setIsActive(mstTaCoveragePlanRequest.getIsActive().toUpperCase());
		this.mstTaCoveragePlanRepo.saveAndFlush(mstTaCoveragePlan);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse deleteMstTaCoveragePlan(String coverageCode) throws MasterServiceApiException {
		MstTaCoveragePlan mstTaCoveragePlan = this.mstTaCoveragePlanRepo.findByCoverageCode(coverageCode);
		if (mstTaCoveragePlan == null)
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
		mstTaCoveragePlan.setIsActive(ConstantUtils.N_FLAG);
		this.mstTaCoveragePlanRepo.saveAndFlush(mstTaCoveragePlan);
		response.setStatus(ResultCode.SUCCESS);
		return response;
	}

	public GenericResponse searchMstTaCoveragePlan(String coverageCode) throws MasterServiceApiException {
		MstTaCoveragePlan mstTaCoveragePlan = this.mstTaCoveragePlanRepo
				.findByCoverageCodeAndIsActiveEquals(coverageCode, ConstantUtils.Y_FLAG);
		if (mstTaCoveragePlan == null)
			throw new MasterServiceApiDataNotFoundException(ResultCode.DATA_NOT_FOUND);

		GenericResponse response = new GenericResponse();
		MstTaCoveragePlanResponseController mstTaCoveragePlanResponse = new MstTaCoveragePlanResponseController();
		BeanUtils.copyProperties(mstTaCoveragePlan, mstTaCoveragePlanResponse);
		response.setStatus(ResultCode.SUCCESS);
		response.setData(mstTaCoveragePlanResponse);
		return response;
	}
}
