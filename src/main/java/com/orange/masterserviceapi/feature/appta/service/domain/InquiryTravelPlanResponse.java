package com.orange.masterserviceapi.feature.appta.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InquiryTravelPlanResponse {
    @JsonProperty("lst_travel_plan_detail")
    private List<TravelPlan> lstTravelPlanDetail;

    @JsonProperty("lst_travel_coverage_plan")
    private List<TravelCoveragePlan> lstTravelCoveragePlan;

   

}
