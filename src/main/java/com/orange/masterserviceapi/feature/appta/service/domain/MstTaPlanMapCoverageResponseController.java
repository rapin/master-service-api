package com.orange.masterserviceapi.feature.appta.service.domain;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class MstTaPlanMapCoverageResponseController {

    private String planCode;

    private String coveragePlanCode;

    private String coveragePlanDetailCode;

    private double price;
    
}
