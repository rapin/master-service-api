package com.orange.masterserviceapi.feature.appta.controller;

import com.orange.masterserviceapi.feature.appta.controller.domain.MstTaCoveragePlanDetailRequestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.feature.appta.service.MasterTaCoveragePlanDetailService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Api(tags = "ta-coverage-plans-detail", hidden = true)
@RequestMapping(path = "/v1/ta-coverage-plans-detail")
public class MasterTaCoveragePlanDetailController {

	@Autowired
	private MasterTaCoveragePlanDetailService masterTaCoveragePlanDetailService;

	@ApiOperation(value = "Insert Master Travel Coverage Plan Detail")
	@PostMapping
	public ResponseEntity<GenericResponse> insertMstTaCoveragePlanDetail(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
										@RequestBody MstTaCoveragePlanDetailRequestController mstTaCoveragePlanDetailRequest) throws MasterServiceApiException {
		GenericResponse response = this.masterTaCoveragePlanDetailService.insertMstTaCoveragePlanDetail(mstTaCoveragePlanDetailRequest);
		ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(response);
		return responseEntity;
	}
	
	@ApiOperation(value = "Update Master Travel Coverage Plan Detail")
	@PutMapping
	public ResponseEntity<GenericResponse> updateMstTaCoveragePlanDetail(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
			 												@RequestBody MstTaCoveragePlanDetailRequestController mstTaCoveragePlanDetailRequest) throws MasterServiceApiException{
	
	    GenericResponse response = this.masterTaCoveragePlanDetailService.updateMstTaCoveragePlanDetail(mstTaCoveragePlanDetailRequest);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
	}
	
	@ApiOperation(value = "Delete Master Travel Coverage Plan Detail")
	@DeleteMapping("/{coverage_detail_code}")
	public ResponseEntity<GenericResponse> deleteMstTaCoveragePlanDetail(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
															@PathVariable("coverage_detail_code") String coverageDetailCode) throws MasterServiceApiException{
	
	    GenericResponse response = this.masterTaCoveragePlanDetailService.deleteMstTaCoveragePlanDetail(coverageDetailCode);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
	}
	
	@ApiOperation(value = "Search Master Travel Coverage Plan Detail")
    @GetMapping("/{coverage_detail_code}")
    public ResponseEntity<GenericResponse> searchMstTaCoveragePlanDetail(@RequestHeader(name = "X-Correlation-Id", required = true) String correlationId,
    														@PathVariable("coverage_detail_code") String coverageDetailCode) throws MasterServiceApiException {
    	GenericResponse response = this.masterTaCoveragePlanDetailService.searchMstTaCoveragePlanDetail(coverageDetailCode);
        ResponseEntity<GenericResponse> responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        return  responseEntity;
    }
}
