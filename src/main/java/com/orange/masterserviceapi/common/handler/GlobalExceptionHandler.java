package com.orange.masterserviceapi.common.handler;

import com.orange.masterserviceapi.common.exception.MasterServiceApiDataNotFoundException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiDataValidateException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.orange.masterserviceapi.common.exception.MasterServiceApiBusinessException;
import com.orange.masterserviceapi.common.exception.MasterServiceApiException;
import com.orange.masterserviceapi.common.utils.GenericResponse;
import com.orange.masterserviceapi.common.utils.ResultCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(MasterServiceApiException.class)
    public ResponseEntity<GenericResponse> handlerGeneralException(MasterServiceApiException e) {
        log.error("General Error Exception : [{}]",e.getStatus().getRemark());
        GenericResponse response = new GenericResponse();
        response.setStatus(ResultCode.SYSTEM_ERROR);
        return new ResponseEntity<>(response, HttpStatus.SERVICE_UNAVAILABLE);
    }

	@ExceptionHandler(MasterServiceApiDataNotFoundException.class)
	public ResponseEntity<GenericResponse> dataNotFoundException(MasterServiceApiDataNotFoundException e){
		log.error("Data Not Found.[{}]",e.getStatus().getRemark());
		GenericResponse response = new GenericResponse();
		response.setStatus(ResultCode.DATA_NOT_FOUND);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(MasterServiceApiDataValidateException.class)
	 public ResponseEntity<GenericResponse> dataValidateException(MasterServiceApiDataValidateException e){
		log.error("Data Validate Exception.[{}]",e.getStatus().getRemark());
		return new ResponseEntity<>(new GenericResponse(e.getStatus()),HttpStatus.BAD_REQUEST);
	}
	 
	@ExceptionHandler(MasterServiceApiBusinessException.class)
	 public ResponseEntity<GenericResponse> dataBusinessException(MasterServiceApiBusinessException e){
		log.error("Business Invalid Exception : [{}]",e.getStatus().getRemark());
		return new ResponseEntity<>(new GenericResponse(e.getStatus()),HttpStatus.BAD_REQUEST);
	}
}