package com.orange.masterserviceapi.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CustomWebConfig implements WebMvcConfigurer {

  @Autowired
  private CustomHandleInterceptor customHandleInterceptor;

  @Override
  public void addInterceptors(final InterceptorRegistry registry) {
    registry.addInterceptor(customHandleInterceptor)
            .addPathPatterns("/**")
            .excludePathPatterns("/**/info")
            .excludePathPatterns("/**/health");
  }
}