package com.orange.masterserviceapi.common.config;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.security.cert.X509Certificate;

@Configuration
public class RestTemplateConfig {

    @Bean
    @Qualifier("orangeRouterRestTemplate")
    public RestTemplate orangeRouterRestTemplate() throws Exception{

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory httpComponentFactory = this.buildFactoryTrustAllCert();

        httpComponentFactory.setConnectTimeout(1000);
        httpComponentFactory.setReadTimeout(2000);

        restTemplate.setRequestFactory(httpComponentFactory);
        return restTemplate;
    }

    @Bean
    @Qualifier("mangoRouterRestTemplate")
    public RestTemplate mangoRouterRestTemplate() throws Exception{

        RestTemplate restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory httpComponentFactory = this.buildFactoryTrustAllCert();

        httpComponentFactory.setConnectTimeout(3000);
        httpComponentFactory.setReadTimeout(1000);

        restTemplate.setRequestFactory(httpComponentFactory);
        return restTemplate;
    }

    private HttpComponentsClientHttpRequestFactory buildFactoryTrustAllCert() throws Exception {
        SSLContextBuilder builder = SSLContexts.custom();
        builder.loadTrustMaterial(null, new TrustStrategy() {
            @Override
            public boolean isTrusted(X509Certificate[] chain, String authType) {
                return true;
            }
        });

        SSLContext sslContext = builder.build();
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext,
                new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("https", sslSocketFactory).register("http", new PlainConnectionSocketFactory()).build();

        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager(
                socketFactoryRegistry);
        poolingHttpClientConnectionManager.setMaxTotal(100);
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(150);

        CloseableHttpClient closeableHttpClient = HttpClients.custom()
                .setConnectionManager(poolingHttpClientConnectionManager)
                .setDefaultRequestConfig(RequestConfig.custom().build()).build();
        return new HttpComponentsClientHttpRequestFactory(closeableHttpClient);
    }
}
