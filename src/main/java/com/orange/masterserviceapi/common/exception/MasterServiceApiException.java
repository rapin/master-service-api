package com.orange.masterserviceapi.common.exception;

import com.orange.masterserviceapi.common.utils.Status;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MasterServiceApiException extends Exception {
	private Status status;
	public MasterServiceApiException(Status status) {
		this.status = status;
	}

	public MasterServiceApiException(Status status, String repleteMessage) {
		super(repleteMessage);
		status.setRemark(repleteMessage);
		this.status = status;
	}
}