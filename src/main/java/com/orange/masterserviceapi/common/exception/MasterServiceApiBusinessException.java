package com.orange.masterserviceapi.common.exception;

import com.orange.masterserviceapi.common.utils.Status;

public class MasterServiceApiBusinessException extends MasterServiceApiException {

	public MasterServiceApiBusinessException(Status status) {
		super(status);
	}

	public MasterServiceApiBusinessException(Status status, String repleteMessage) {
		super(status, repleteMessage);
	}
}