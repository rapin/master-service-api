package com.orange.masterserviceapi.common.exception;

import com.orange.masterserviceapi.common.utils.Status;

public class MasterServiceApiDataNotFoundException extends MasterServiceApiException {
	public MasterServiceApiDataNotFoundException(Status status) {
		super(status);
	}

	public MasterServiceApiDataNotFoundException(Status status, String repleteMessage) {
		super(status, repleteMessage);
	}
}