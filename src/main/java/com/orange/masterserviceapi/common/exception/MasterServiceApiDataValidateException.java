package com.orange.masterserviceapi.common.exception;

import com.orange.masterserviceapi.common.utils.Status;

public class MasterServiceApiDataValidateException extends MasterServiceApiException {

	public MasterServiceApiDataValidateException(Status status) {
		super(status);
	}
}